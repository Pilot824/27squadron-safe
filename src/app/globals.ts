/**
 * Created by andrew on 09/05/17.
 */

import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { NativeDateAdapter } from '@angular/material';

@Injectable()
export class Globals {

	// Saw this being done from DevLift's code. Why?
	serverBaseURL: string = environment.serverBaseURL;

	versionNumber = '0.3.8';

}

/**
 * Availability status codes that are used in the database.
 */
export enum AvailabilityStatus {
	YES = 1,
	NO = -1,
	UNSET = 0
}

export enum AttendanceStatus {
	PRESENT = 1,
	ABSENT = -1,
	PRESENTLATE = 2
}

/**
 * Displays whether or not an event is public or private.
 * Private events are shown only to members in the members sections.
 * Public events are shown to the cadets in the private section, and are shown to the public on the home page, and under the about section.
 */
export enum PublicStatus {
	PUBLIC = 1,
	PRIVATE = 0
}

/**
 * Parent permission is for classifying if an event requires a cadet to receive parent permission.
 */
export enum ParentPermission {
	REQURED = 1,
	OPTIONAL = 0
}

export const StaffUniforms = [
	'1',
	'1A',
	'1B',
	'1C',
	'1D',
	'2',
	'2A',
	'2B',
	'2C',
	'2D',
	'3',
	'3A',
	'3B',
	'3C',
	'3D',
	'4',
	'5',
	'5A',
	'5B',
	'5C',
	'5D',
	'Combats',
	'Flight Suit',
	'Civillian Attire'
];

export const CadetUniforms = [
	'C1',
	'C2',
	'C3',
	'C4',
	'C5',
	'C6',
	'C7',
	'C8',
	'Combats / Field Training Attire',
	'Civillian Attire'
];

export class MyDateAdapter extends NativeDateAdapter {
	format(date: Date, displayFormat: Object): string {
		if (displayFormat == 'input') {
			let day = date.getDate();
			let month = date.getMonth() + 1;
			let year = date.getFullYear();
			return this._to2digit(day) + '/' + this._to2digit(month) + '/' + year;
		} else {
			return date.toDateString();
		}
	}

	private _to2digit(n: number) {
		return ('00' + n).slice(-2);
	}
}

export const MY_DATE_FORMATS = {
	parse: {
		dateInput: { month: 'short', year: 'numeric', day: 'numeric' }
	},
	display: {
		// dateInput: { month: 'short', year: 'numeric', day: 'numeric' },
		dateInput: 'input',
		monthYearLabel: { year: 'numeric', month: 'short' },
		dateA11yLabel: { year: 'numeric', month: 'long', day: 'numeric' },
		monthYearA11yLabel: { year: 'numeric', month: 'long' },
	}
}
