import { Component, OnInit } from '@angular/core';

@Component (
	{
		selector    : 'app-about',
		templateUrl : 'about.program.page.html',
		styleUrls   : [ 'about.program.page.css' ]
	}
)
export class AboutProgramPage implements OnInit {

	constructor() {
	}

	ngOnInit() {
	}

}
