import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { environment } from '../../../../../environments/environment';
import { UserModel } from '../../../../models/user.model';
import { BaseService } from 'app/services/base/base.service';
import { ToastyService } from 'ng2-toasty';
import { UserService } from 'app/services/user/user.service';

@Injectable()
export class AboutStaffService extends BaseService {

	private dbURL: string;

	constructor(protected http: Http, protected toastyService: ToastyService, private userService: UserService) {

		super(http, toastyService);

		this.dbURL = environment.serverBaseURL + '/server/lib/db_public.php';
	}

	/**
	 * callQuery sends a request to the server given the query string.
	 *
	 * @param query
	 * @returns {Observable<any[]>}
	 */
	private callQuery(query: string): Observable<any[]> {
		return this.http.get(this.dbURL + '?query=' + query)
			.map(response => {
				return this.yahooFix(response);
			})
			.catch(this.handleError);
	}

	/**
	 * Get staff calls returns a list of current staff members
	 * @returns {Observable<UserModel[]>}
	 */
	public getStaff(): Observable<UserModel[]> {
		return this.callQuery('current_staff_list').map(staffList => {
			return staffList.map(staff => {
				staff.rank = this.userService.getRank(staff.rank_id).then(rank => {
					return staff.rank = rank;
				});
				return staff;
			});
		});
	}

	public getPastCOs(): Observable<UserModel[]> {
		return this.callQuery('past_cos');
	}

}
