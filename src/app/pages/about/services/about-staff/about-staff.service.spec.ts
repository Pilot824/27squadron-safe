import { AboutStaffService } from 'app/pages/about/services/about-staff/about.staff.service';
import { inject, TestBed } from '@angular/core/testing';
import { HttpModule } from '@angular/http';
import { ToastyService, ToastyConfig } from 'ng2-toasty';
import { UserService } from 'app/services/user/user.service';
import { NotificationsService } from 'app/services/notifications/notifications-service.service';

describe('About Staff Service', function () {

	let service;

	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				AboutStaffService,
				ToastyService,
				ToastyConfig,
				UserService,
				NotificationsService
			],
			imports: [
				HttpModule
			]
		})
	});

	beforeEach(inject([AboutStaffService], s => {
		this.service = s;
	}));

	it('should initiate', inject([AboutStaffService], (s: AboutStaffService) => {
		expect(s).toBeTruthy();
	}));

	it('should get a staff list', inject([AboutStaffService], (s: AboutStaffService) => {

		return s.getStaff().toPromise().then(res => {
			expect(res).toBeTruthy();
			expect(res.length).toBeGreaterThan(0);
		});

	}));


});
