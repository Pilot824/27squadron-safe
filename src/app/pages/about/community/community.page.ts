import { Component, OnInit } from '@angular/core';
import { EventModel } from '../../../models/event.model';
import { EventsService } from '../../../services/events/events.service';

/**
 * The community page displays content to the public about what air cadets do.
 */
@Component ( {
	selector    : 'app-community',
	templateUrl : 'community.page.html',
	styleUrls   : [ 'community.page.css' ],
	providers   : [ EventsService ]
} )
export class AboutCommunityPage implements OnInit {

	/**
	 * events is the list of events that are displayed on the home page. Event must be public to be displayed
	 */
	events : EventModel[];

	/**
	 * Constructor intializes the array of events as an empty array
	 */
	constructor( private eventsService : EventsService ) {
		this.events = new Array<EventModel> ();
	}

	/**
	 * ngOnInit currently assigns a default value to the event
	 *
	 * TODO: connect community page with event service.
	 */
	ngOnInit() {

		this.eventsService.getEvents ().subscribe ( data => {
			this.events = data.filter( event => {
				return +event.is_public;
			});
		} );
	}

}
