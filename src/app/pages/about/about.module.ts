import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AboutComponent } from "./about.component";
import { AboutProgramPage } from "./program/about.program.page";
import { AboutStaffPage } from "./staff/about.staff.page";
import { AboutSponsorsPage } from "./sponsors/sponsors.page";
import { AboutCommunityPage } from "./community/community.page";
import { AboutRoutingModule } from "./about-routing.module";
import { SharedModule } from "../../components/shared.module";

@NgModule ( {
	imports         : [
		CommonModule,
		AboutRoutingModule,
		SharedModule
	], declarations : [
		AboutComponent,

		AboutProgramPage,
		AboutStaffPage,
		AboutSponsorsPage,
		AboutCommunityPage
	], schemas      : [ CUSTOM_ELEMENTS_SCHEMA ]
} )
export class AboutModule {
}