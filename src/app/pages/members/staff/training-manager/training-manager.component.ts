import { Component, OnInit } from '@angular/core';
import { TrainingEventModel } from 'app/models/event.model';
import { TrainingService } from 'app/services/training/training.service';
import { Observable } from 'rxjs/Observable';
import { UserModel } from 'app/models/user.model';
import { UserService } from 'app/services/user/user.service';
import { MatDialog, MatDialogRef } from '@angular/material';
import { LoadingModalComponent } from 'app/components/loading-modal/loading-modal.component';

@Component({
	selector: 'app-training-manager',
	templateUrl: './training-manager.component.html',
	styleUrls: ['./training-manager.component.css']
})
export class TrainingManagerComponent implements OnInit {

	public classes: TrainingEventModel[];

	constructor(private trainingService: TrainingService, private userService: UserService, private dialog: MatDialog) {
		this.classes = new Array<TrainingEventModel>();
		this.dialog.open(LoadingModalComponent);

	}

	ngOnInit() {
		let trgServiceClassObs = this.trainingService.getAllClasses()
			.subscribe(res => {
				if (res != null) {
					this.classes = res;
					this.dialog.closeAll();
					trgServiceClassObs.unsubscribe();
				}
			});
	}

}
