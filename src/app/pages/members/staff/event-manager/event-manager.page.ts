import { Component, OnInit } from '@angular/core';
import { EventModel } from '../../../../models/event.model';
import { EventsService } from '../../../../services/events/events.service';
import { ToastyService, ToastyConfig } from 'ng2-toasty';
import { ConfirmDeleteComponent } from '../../components/confirm-event-delete/confirm-delete.component';
import { MatDialog, MatDialogRef } from '@angular/material';
import { LoadingModalComponent } from 'app/components/loading-modal/loading-modal.component';
import { Observable } from 'rxjs/Observable';

@Component({
	selector: 'app-event-manager',
	templateUrl: 'event-manager.page.html',
	styleUrls: ['event-manager.page.css'],
	providers: [EventsService]
})
export class EventManagerPage implements OnInit {

	events: EventModel[];
	upcomingEvents: EventModel[];
	pastEvents: EventModel[];

	pastEventLimit = 10;

	/**
	 * Initiates variables
	 * @param eventsService
	 */
	constructor(private eventsService: EventsService, private toastyService: ToastyService, private toastyConfig: ToastyConfig, public dialog: MatDialog) {
		this.events = Array<EventModel>();
		this.toastyConfig.theme = 'material';
		this.dialog.open(LoadingModalComponent);
	}

	/**
	 * Calls appropriate function to initiate page
	 */
	ngOnInit() {

		this.populateEvents().subscribe(() => {
			this.dialog.closeAll();
		});
	}

	/**
	 * calls eventsService function to retrieve all events
	 */
	populateEvents(): Observable<any> {
		return this.eventsService.getEvents().map(events => {
			this.events = events.map(event => {
				(<any>event).staffRequirementMet = this.eventsService.staffRequirementMet(event);
				return event;
			});

			this.upcomingEvents = [];
			this.pastEvents = [];

			this.events.forEach(event => {

				if (event.end_date_time != '0000-00-00 00:00:00') {

					if (Date.parse(event.end_date_time) >= Date.now()) {
						this.upcomingEvents.push(event);
					} else if (Date.parse(event.end_date_time) < Date.now()) {
						this.pastEvents.push(event);
					}

				} else {

					if (Date.parse(event.start_date_time) >= Date.now()) {
						this.upcomingEvents.push(event);
					} else if (Date.parse(event.start_date_time) < Date.now()) {
						this.pastEvents.push(event);
					}

				}

			})

			this.pastEvents = this.pastEvents.slice(0, this.pastEventLimit);
		});
	}

	deleteEvent(event: EventModel) {
		let dialogRef = this.dialog.open(ConfirmDeleteComponent);
		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				try {
					this.eventsService.deleteEvent(event).subscribe(
						res => {
							console.log('event deleted');
						},
						error => {
							throw new error(error);
						},
						() => {
							this.populateEvents().subscribe();
						}
					);
				} catch (e) {
					this.toastyService.error(e.toString());
				}
			}
		});

	}

	addEvent() {
		try {
			let newEvent = new EventModel;

			newEvent.event_title = '';
			newEvent.event_title = '';


			this.eventsService.newEvent(newEvent).subscribe(
				event => {
					if (typeof event.error !== 'undefined') {
						throw new Error(event.error);
					}

				},
				error => {
					throw new Error(error)
				},
				() => {
					this.populateEvents().subscribe();
				});
		} catch (e) {
			this.toastyService.error(e.toString());
			this.toastyService.error(e.toString());
		}
	}

}
