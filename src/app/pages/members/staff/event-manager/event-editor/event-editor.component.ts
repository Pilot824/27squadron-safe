import { Component, OnInit } from '@angular/core';
import { EventModel, EventAvailModel } from '../../../../../models/event.model';
import { ActivatedRoute, Router } from '@angular/router';
import { EventsService } from '../../../../../services/events/events.service';
import { ToastyConfig, ToastyService } from 'ng2-toasty';
import { FormGroup, FormControl } from '@angular/forms';
import { QuestionBase } from 'app/components/questions/question-base';
import { Header2Question } from 'app/components/questions/question-header2';
import { TextboxQuestion } from 'app/components/questions/question-textbox';
import { Observable } from 'rxjs/Observable';
import { environment } from 'environments/environment';
import { QuestionControlService } from 'app/services/question-control/question-control.service';
import { Header1Question } from 'app/components/questions/question-header1';
import { NumberQuestion } from 'app/components/questions/question-number';
import { HiddenQuestion } from 'app/components/questions/question-hidden';
import { CheckboxQuestion } from 'app/components/questions/question-checkbox';
import { DropdownQuestion } from 'app/components/questions/question-dropdown';
import { DateQuestion } from 'app/components/questions/question-date';
import { TimeQuestion } from 'app/components/questions/question-time';
import { StaffUniforms, CadetUniforms } from 'app/globals';

@Component({
	selector: 'app-event-editor',
	templateUrl: 'event-editor.component.html',
	styleUrls: ['event-editor.component.css'],
	providers: [EventsService]
})
export class EventEditorComponent implements OnInit {

	event: EventAvailModel;

	public staff_uniforms: any[];
	public cadet_uniforms: any[];

	public eventFormGroup: FormGroup;
	public questions: QuestionBase<any>[];

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private eventsService: EventsService,
		private toastyService: ToastyService,
		private toastyConfig: ToastyConfig,
		private questionControlService: QuestionControlService
	) {

		this.event = new EventAvailModel;
		this.toastyConfig.theme = 'material';

		this.staff_uniforms = StaffUniforms;

		this.cadet_uniforms = CadetUniforms;
	}

	ngOnInit() {

		this.updateForm(null);

		this.getEvent().subscribe(event => {

			if (!!(event)) {
				this.updateForm(event);
			}
		});
	}

	getEvent(): Observable<EventModel> {
		// retrieving route params
		return this.route.params.flatMap(params => {

			// if id is passed in, retrieve event
			if (+params['id'] != null) {
				return this.eventsService.getEvent(+params['id']).map(event => {
					if (event) {
						return this.event = event;
					}
				});
			}
		});
	}

	updateEvent(jsonString?: string): void {

		try {

			let event: EventModel;

			if (jsonString != null && jsonString != '') {
				let tmpevent = JSON.parse(jsonString);

				let date_start_parsed = new Date(tmpevent.start_date);
				let date_end_parsed = new Date(tmpevent.end_date);
				let time_start_parsed = new Date(tmpevent.start_time);
				let time_end_parsed = new Date(tmpevent.end_time);

				event = tmpevent;

				event.start_date_time = new Date(
					date_start_parsed.getFullYear(),
					date_start_parsed.getMonth(),
					date_start_parsed.getDate(),
					time_start_parsed.getHours(),
					time_start_parsed.getMinutes()).toLocaleString();

				event.end_date_time = new Date(
					date_end_parsed.getFullYear(),
					date_end_parsed.getMonth(),
					date_end_parsed.getDate(),
					time_end_parsed.getHours(),
					time_end_parsed.getMinutes()).toLocaleString();
			} else {
				event = this.event;
			}

			if (event.event_id == null || (<any>event.event_id) == '') {
				throw new Error('Event ID Was not set');
			}

			this.eventsService.setEvent(event).subscribe(res => {

				try {

					if (res == null) {
						throw new Error('Response from the server was null');
					}

					this.router.navigate(['/members/staff/event-manager']);

				} catch (e) {

					console.error(e.toString());
					console.error('Res response: ', res ? res.error : res.toString());
					this.toastyService.error(e.toString());

				}
			});
		} catch (e) {
			console.error(e.toString());
		}
	}

	deleteEvent(): void {
		try {
			this.eventsService.deleteEvent(this.event);
		} catch (e) {
			this.toastyService.error(e.toString());
		}
	}

	updateForm(event?: EventAvailModel) {

		this.questions = [

			new Header2Question({
				label: event ? event.event_title + ' Details' : 'Event Details',
				order: 0
			}),

			new Header2Question({
				label: '<small>Event Information</small>',
				order: 1
			}),

			new HiddenQuestion({
				key: 'event_id',
				value: event ? event.event_id.toString() : '',
				order: 2,
				hidden: true,
			}),

			new TextboxQuestion({
				key: 'event_title',
				label: 'Event Title',
				value: event ? event.event_title : '',
				required: true,
				order: 3,
				disabled: environment.disableForms
			}),

			new TextboxQuestion({
				key: 'event_description',
				label: 'Event Description',
				value: event ? event.event_description : '',
				required: false,
				order: 4,
				disabled: environment.disableForms,
				multiline: true
			}),

			new DateQuestion({
				key: 'start_date',
				label: 'Start Date',
				value: event ? new Date(event.start_date_time).toISOString() : '',
				required: true,
				order: 5,
				disabled: environment.disableForms
			}),

			new TimeQuestion({
				key: 'start_time',
				label: 'Start Time',
				value: event ? event.start_date_time : '',
				required: true,
				order: 6,
				disabled: environment.disableForms
			}),

			new DateQuestion({
				key: 'end_date',
				label: 'End Date',
				value: event ? new Date(event.end_date_time).toISOString() : '',
				required: true,
				order: 6,
				disabled: environment.disableForms
			}),

			new TimeQuestion({
				key: 'end_time',
				label: 'End Time',
				value: event ? event.end_date_time : '',
				required: true,
				order: 6,
				disabled: environment.disableForms
			}),

			new Header2Question({
				label: 'Site Handling',
				order: 7
			}),

			new CheckboxQuestion({
				key: 'get_availabilities',
				label: 'Get Availabilities',
				value: event ? !!(event.get_availabilities) : false,
				required: false,
				order: 9,
				disabled: environment.disableForms
			}),

			new CheckboxQuestion({
				key: 'parent_permission',
				label: 'Parent Permission',
				value: event ? !!(event.parent_permission) : false,
				required: false,
				order: 10,
				disabled: environment.disableForms
			}),

			new CheckboxQuestion({
				key: 'is_public',
				label: 'Public Event',
				value: event ? !!(event.is_public) : false,
				required: false,
				order: 11,
				disabled: environment.disableForms
			}),

			new CheckboxQuestion({
				key: 'can_change',
				label: 'Allow users to change availabilities after submission',
				value: event ? !!(event.can_change) : false,
				required: false,
				order: 12,
				disabled: environment.disableForms
			}),

			new CheckboxQuestion({
				key: 'gen_permission_form',
				label: 'Generate Permission Form',
				value: event ? !!(event.gen_permission_form) : false,
				required: false,
				order: 13,
				disabled: environment.disableForms
			}),

			new NumberQuestion({
				key: 'max_cadet_count',
				label: 'Max Number of Cadets',
				value: event ? event.max_cadet_count : 0,
				min: 0,
				max: 100,
				required: false,
				order: 14,
				disabled: environment.disableForms
			}),

			new DropdownQuestion({
				key: 'cadet_uniform',
				label: 'Cadet Uniform',
				value: event ? event.cadet_uniform : '',
				options: this.cadet_uniforms.map((uniform, index) => {
					return { key: uniform, value: uniform };
				}),
				order: 15
			}),

			new DropdownQuestion({
				key: 'staff_uniform',
				label: 'Staff Uniform',
				value: event ? event.staff_uniform : '',
				options: this.staff_uniforms.map((uniform, index) => {
					return { key: uniform, value: uniform };
				}),
				order: 16
			}),

			new TextboxQuestion({
				key: 'additional_permission_form_notes',
				label: 'Additional Permission Form Notes',
				value: event ? event.additional_permission_form_notes : '',
				required: false,
				disabled: environment.disableForms
			}),
		];

		// this.questions.sort((a, b) => a.order - b.order);

		this.eventFormGroup = this.questionControlService.toFormGroup(this.questions);
	}

}
