import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventManagerPage } from './event-manager/event-manager.page';
import { MemberManagerPage } from './member-manager/member-manager.page';
import { EventEditorComponent } from './event-manager/event-editor/event-editor.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { SignInAuthenticatorPage } from './sign-in-authenticator/sign-in-authenticator.page';
import { EventRosterComponent } from './event-manager/event-roster/event-roster.component';
import { ModalModule } from 'ngx-bootstrap';
import { SharedModule } from 'app/components/shared.module';
import { TrainingManagerComponent } from './training-manager/training-manager.component';
import { TrainingEventEditorComponent } from './training-manager/training-event-editor/training-event-editor.component';
// import { Ng2DragDropModule } from 'ng2-drag-drop';

@NgModule({
	imports: [
		CommonModule,
		SharedModule,
		RouterModule,
		ModalModule.forRoot(),
		FormsModule,
		// Ng2DragDropModule
	], declarations: [
		EventManagerPage,
		EventEditorComponent,
		SignInAuthenticatorPage,
		EventRosterComponent,
		MemberManagerPage,
		TrainingManagerComponent,
		TrainingEventEditorComponent
	],
	schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class StaffModule {
}
