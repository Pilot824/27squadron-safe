import { Component, OnInit } from '@angular/core';
import { UserModel } from '../../../../models/user.model';
import { UserService } from '../../../../services/user/user.service';
import { RankModel } from '../../../../models/rank.model';
import { ToastyConfig, ToastyService } from 'ng2-toasty';
import { AuthUserService } from '../../../../services/auth-user/auth-user.service';
import { AuthMember } from '../../../../services/auth-user/auth-member.service';
import { MatDialog, MatDialogRef } from '@angular/material';
import { LoadingModalComponent } from 'app/components/loading-modal/loading-modal.component';

@Component({
	selector: 'app-profile-manager',
	templateUrl: './profile-manager.page.html',
	styleUrls: ['./profile-manager.page.css'],
	providers: [
		AuthUserService,
		UserService
	]
})
export class ProfileManagerPage implements OnInit {

	user: UserModel;

	constructor(protected authUserService: AuthUserService, protected userService: UserService,
		protected toastyService: ToastyService, private toastyConfig: ToastyConfig,
		private authMember: AuthMember, private dialog: MatDialog) {

		this.dialog.open(LoadingModalComponent);

		this.user = new UserModel();
		this.user.rank = new RankModel();
		this.user.rank.rank_text_full = '';
		this.toastyConfig.theme = 'material';

	}

	ngOnInit() {
		this.authUserService.invalidateStoredUser();

		let getUserObs = this.authUserService.getUser(this.authMember.getMemberId()).subscribe(user => {
			if (typeof user !== 'undefined') {
				this.user = user;
				this.dialog.closeAll();
				getUserObs.unsubscribe();
			}
		});
	}

}
