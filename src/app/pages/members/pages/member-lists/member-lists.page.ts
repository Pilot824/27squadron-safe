import { Component, OnInit } from '@angular/core';
import { UserModel, SearchUserModel } from 'app/models/user.model';
import { UserService } from 'app/services/user/user.service';

@Component({
	selector: 'app-member-lists',
	templateUrl: './member-lists.page.html',
	styleUrls: ['./member-lists.page.css']
})
export class MemberListsPage implements OnInit {

	public users: SearchUserModel[];
	public undefinedPlaceholderText = `Undefined`;

	constructor(private userService: UserService) { }

	ngOnInit() {
		this.userService.listOfUsers().subscribe(users => {
			this.users = users;
		});
	}

}
