import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { EventAvailModel } from '../../../../models/event.model';
import { AvailabilitiesService } from '../../../../services/availabilities/availabilities.service';
import { AuthUserService } from '../../../../services/auth-user/auth-user.service';
import { FacebookService } from '../../../../services/facebook/facebook.service';
import { NotificationsService } from 'app/services/notifications/notifications-service.service';
import { NotificationModel } from 'app/models/notification.model';
import { MatDialog, MatDialogRef } from '@angular/material';
import { LoadingModalComponent } from 'app/components/loading-modal/loading-modal.component';

/**
 * The members dashboard should show a snapshot of the upcoming events for the member and any unread announcements.
 *
 * TODO: Add in summary of upcoming events
 * TODO: Add in unread announcements list
 */
@Component({
	selector: 'app-members-login',
	templateUrl: 'dashboard.page.html',
	styleUrls: ['dashboard.page.css'],
	providers: [
		AvailabilitiesService,
		AuthUserService,
		FacebookService
	]
})
export class MemberDashboardPage implements OnInit {

	attendingEventList: EventAvailModel[];

	/**
	 * Constructor does nothing at the moment
	 */
	// tslint:disable-next-line:max-line-length
	constructor(
		private availabilitiesService: AvailabilitiesService,
		private authUserService: AuthUserService,
		private facebookService: FacebookService,
		private notifService: NotificationsService,
		private dialog: MatDialog,
		private cdr: ChangeDetectorRef) {
			this.dialog.open(LoadingModalComponent);
	}

	ngOnInit() {
		this.getAttendingEvents().subscribe(() => {
			this.dialog.closeAll();
		});
	}

	/**
	 * Get list of events that the user has said they would attend
	 */
	getAttendingEvents() {

		return this.authUserService.getUser().map(user => {

			if (user.member_id == null) {
				throw new Error('user returned a null value!');
			}

			this.availabilitiesService.getMemberEventAvailabilities(user.member_id).subscribe(res => {
				setTimeout(() => this.attendingEventList = res, 0)
			});
		});
	}

	testNewNotif() {

		let tmpNotif = new NotificationModel;

		tmpNotif.text = 'New Test';
		tmpNotif.style = 'success';

		this.notifService.addNotification(tmpNotif);
	}

}
