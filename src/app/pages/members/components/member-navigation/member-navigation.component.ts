import { Component, OnInit } from '@angular/core';
import { NavbarLinkModel } from '../../../../models/navbarlink.model';
import { NavigationService } from '../../../../services/navigation/navigation.service';
import { AuthStaffService } from 'app/services/auth-user/auth-staff.service';

@Component({
	selector: 'app-member-navigation',
	templateUrl: 'member-navigation.component.html',
	styleUrls: ['member-navigation.component.css'],
	providers: [NavigationService]
})

/**
 * The member nagivation component generates an additional navbar for when users are on the members page.
 */

export class MemberNavigationComponent implements OnInit {

	/**
	 * navigationMenu contains all of the links
	 */
	public navigationMenu: NavbarLinkModel[];

	/**
	 * constructor initializes navigationMeny with a blank array.
	 * @param navService
	 */
	constructor(private navService: NavigationService, private authStaffService: AuthStaffService) {
		this.navigationMenu = new Array<NavbarLinkModel>();
	}

	/**
	 * ngOnInit generates the navigationMenu array from {@link NavigationService}
	 */
	ngOnInit() {

		this.navService.getMemberNav().forEach(link => {
			this.navigationMenu.push(link);
		});

		this.authStaffService.canActivate().subscribe(showStaffNav => {
			if (showStaffNav) {
				this.navService.getStaffNav().forEach(link => {
					this.navigationMenu.push(link);
				});
			}
		});
	}

}
