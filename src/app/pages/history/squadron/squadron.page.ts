import { Component, OnInit } from "@angular/core";
import { UserModel } from "../../../models/user.model";
import { AboutStaffService } from "../../about/services/about-staff/about.staff.service";

@Component ( {
	selector    : 'app-squadron',
	templateUrl : 'squadron.page.html',
	styleUrls   : [ 'squadron.page.css' ],
	providers   : [ AboutStaffService ]
} )
export class HistorySquadronPage implements OnInit {

	pastCOs : UserModel[];

	constructor( private aboutStaffService : AboutStaffService ) {
		this.pastCOs = new Array<UserModel> ();
	}

	ngOnInit() {
		this.getPastCOs ();
	}

	private getPastCOs() : void {
		this.aboutStaffService.getPastCOs ()
			.subscribe ( list => {
				if ( list ) {
					this.pastCOs = list;
				}
			} );
	}

}
