import { Component, OnInit } from '@angular/core';
import { UserModel } from '../../../models/user.model';
import { UserService } from '../../../services/user/user.service';
import { RankModel } from '../../../models/rank.model';
import { QuestionBase } from 'app/components/questions/question-base';
import { Header2Question } from 'app/components/questions/question-header2';
import { TextboxQuestion } from 'app/components/questions/question-textbox';
import { PasswordQuestion } from 'app/components/questions/question-password';
import { ButtonQuestion } from 'app/components/questions/question-button';
import { environment } from 'environments/environment';
import { Header1Question } from 'app/components/questions/question-header1';
import { QuestionControlService } from 'app/services/question-control/question-control.service';
import { FormGroup } from '@angular/forms';
import { ToastyService, ToastOptions } from 'ng2-toasty';
import { Router } from '@angular/router';

@Component({
	selector: 'app-new-account',
	templateUrl: './new-account.page.html',
	providers: [UserService]
})
export class NewAccountPage implements OnInit {

	newAccountForm: FormGroup;
	questions: QuestionBase<any>[];

	constructor(
		private userService: UserService,
		private questionControlService: QuestionControlService,
		private toasty: ToastyService,
		private router: Router
	) {
		this.updateForm();
	}

	ngOnInit() {
		this.updateForm();
	}

	updateForm() {
		this.questions = [

			new Header1Question({
				label: 'Account Creation',
				order: 0
			}),

			new TextboxQuestion({
				key: 'fname',
				label: 'First Name',
				value: '',
				required: true,
				order: 1,
				disabled: environment.disableForms
			}),


			new TextboxQuestion({
				key: 'lname',
				label: 'Last Name',
				value: '',
				required: true,
				order: 2,
				disabled: environment.disableForms
			}),


			new TextboxQuestion({
				key: 'email',
				label: 'E-Mail',
				value: '',
				required: true,
				order: 3,
				isEmail: true,
				disabled: environment.disableForms
			}),

			new PasswordQuestion({
				key: 'password',
				label: 'Password',
				required: true,
				order: 4,
				disabled: environment.disableForms
			}),

			new PasswordQuestion({
				key: 'confirmPassword',
				label: 'Confirm Password',
				required: true,
				order: 5,
				disabled: environment.disableForms
			})
		];

		this.questions.sort((a, b) => a.order - b.order);

		this.newAccountForm = this.questionControlService.toFormGroup(this.questions);
	}

	createNewAccount(formJSONData: string) {
		let newUserSub = this.userService.updateUser(JSON.parse(formJSONData))
			.debounceTime(1000)
			.subscribe(newUser => {
				if (!!newUser) {
					if (!!(newUser.error)) {
						this.toasty.error(newUser.error);
					} else {
						this.toasty.success({
							title: 'Account Created',
							msg: 'See a staff member to initialize your account',
							timeout: 50000,
							theme: 'material',
						});
					}
					newUserSub.unsubscribe();
					this.router.navigate(['login']);
				}
			});
	}

}
