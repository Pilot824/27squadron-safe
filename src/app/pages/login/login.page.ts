import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastyConfig, ToastyService, ToastOptions } from 'ng2-toasty';
import { AuthMember } from '../../services/auth-user/auth-member.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { QuestionBase } from 'app/components/questions/question-base';
import { QuestionControlService } from 'app/services/question-control/question-control.service';
import { TextboxQuestion } from 'app/components/questions/question-textbox';
import { PasswordQuestion } from 'app/components/questions/question-password';
import { Header1Question } from 'app/components/questions/question-header1';
import { Header2Question } from 'app/components/questions/question-header2';
import { environment } from 'environments/environment';
import { ButtonQuestion } from 'app/components/questions/question-button';
import { UserService } from 'app/services/user/user.service';

/**
 * The login page allows members to login with a username and password. Provides access to the members module.
 */
@Component({
	selector: 'app-members-login',
	templateUrl: 'login.page.html',
	styleUrls: ['login.page.css'],
})
export class MembersLoginPage implements OnInit {

	public loading: boolean;
	public loginFormGroup: FormGroup;
	public questions: QuestionBase<any>[];

	/**
	 * Constructor provides access to the angular's router module.
	 * @param router
	 */
	constructor(
		private router: Router,
		private toastyService: ToastyService,
		private authMember: AuthMember,
		private userService: UserService,
		private toastyConfig: ToastyConfig,
		private formBuilder: FormBuilder,
		private questionControlService: QuestionControlService
	) {
		this.toastyConfig.theme = 'material';

		this.loading = false;
	}

	/**
	 * ngOnInit does nothing at the moment.
	 */
	ngOnInit() {
		this.questions = [

			new Header2Question({
				label: 'Members Login Page'
			}),

			new TextboxQuestion({
				key: 'username',
				label: 'E-Mail',
				value: '',
				required: true,
				order: 2,
				isEmail: true,
				disabled: environment.disableForms
			}),

			new PasswordQuestion({
				key: 'password',
				label: 'Password',
				required: true,
				order: 3,
				disabled: environment.disableForms
			}),

			new ButtonQuestion({
				label: 'Create New Account',
				value: '/join/newaccount',
				order: 4,
				disabled: environment.disableForms
			}),

			new ButtonQuestion({
				label: 'Forgot Password',
				value: '/forgotpassword',
				order: 5,
				disabled: environment.disableForms
			})
		];

		this.questions.sort((a, b) => a.order - b.order);

		this.loginFormGroup = this.questionControlService.toFormGroup(this.questions);
	}

	// tslint:disable-next-line:use-life-cycle-interface
	ngAfterContentInit() {
		if (!!(this.userService.getStoredToken())) {
			this.authMember.logout();
		}
	}

	/**
	 * Function that is called when the user attempts to login.
	 */
	loginAction(formJson: string) {

		let formData = JSON.parse(formJson);
		this.loading = true;

		let badLoginToast: ToastOptions = {
			title: 'Failed Login',
			msg: 'Check your username, password, and initialize your account with an officer'
		};

		if (formData != null) {

			try {

				if (this.authMember == null) {
					throw new Error('authMember is undefined');
				}


				// Need to setup switch Mapping
				let loginSubscriber = this.authMember.updateToken(formData.username, formData.password)
					.flatMap(response => {
						return this.authMember.canActivate();
					});

				loginSubscriber.subscribe(loginStatus => {
					if (loginStatus) {
						this.toastyService.success('Logged in!');
						this.router.navigateByUrl('/members/dashboard');
						this.loading = false;
					} else {
						this.loading = false;
						this.toastyService.error(badLoginToast);
					}
				}, error => {
					this.loading = false;
					this.toastyService.error(badLoginToast);
				}, () => {
					this.loading = false;
				});

			} catch (e) {
				this.toastyService.error(e.toString());
				console.error(e);
				this.loading = false;
			}

		}

	}

}
