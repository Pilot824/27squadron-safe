import { RankModel } from './rank.model';
import { UserModel } from 'app/models/user.model';

export class GroupModel {
	group_id : number;
	title : string;
	description : string;
	/**
	 * Officer / Cadet of Primary Interest
	 */
	PI? : UserModel;
	active_start : string;
	active_end : string;
	isMember? : boolean;
}
