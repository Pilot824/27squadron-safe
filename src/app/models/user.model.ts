import { RankModel } from './rank.model';
import { AttendanceStatus, AvailabilityStatus } from '../globals';
/**
 * Created by andrew on 11/04/17.
 */

export class BaseUserModel {
	member_id?: number;
	fname: string;
	lname: string;
	rank_id?: number;	// Used as a temporary placeholder
	rank: RankModel;
}

export class UserModel extends BaseUserModel {
	gender?: string;
	email?: string;
	phone_number?: number;
	profile_image_filename: string;
	archived: boolean;
	element?: string;
	branch?: string;
	reportsTo?: number;
	position_id?: number;
	title?: string;
	initialized: boolean;

	// Cadet Information
	level? : number;
}

export class SearchUserModel extends UserModel {
	filtered: boolean; // Hide if true
	showChallenge: boolean;
	status: AttendanceStatus;
}

export class AttendanceUserModel extends BaseUserModel {
	status: number;
}
