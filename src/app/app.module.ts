import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HomePage } from './pages/home/home.page';
import { AppRoutingModule } from './app-routing.module';
import { MembersModule } from './pages/members/members.module';
import { PageNotFoundPage } from './pages/general/page-not-found/page-not-found.page';
import { AboutModule } from './pages/about/about.module';
import { SharedModule } from './components/shared.module';
import { MembersLoginPage } from './pages/login/login.page';
import { StaffModule } from './pages/members/staff/staff.module';
import { HistoryModule } from './pages/history/history.module';
import { RouterModule } from '@angular/router';
import { JoinModule } from './pages/join/join.module';
import { ToastyModule } from 'ng2-toasty';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ServicesModule } from './services/';
import { ChangeLogPage } from './pages/changelog/changelog.page';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ForgotPasswordPage } from 'app/pages/forgot-password/forgot-password.page';
import { TimepickerModule } from 'ngx-bootstrap';

@NgModule({
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		NoopAnimationsModule,
		FormsModule,
		HttpModule,
		AppRoutingModule,
		RouterModule,
		SharedModule,
		MembersModule,
		AboutModule,
		StaffModule,
		HistoryModule,
		JoinModule,
		ToastyModule.forRoot(),
		ModalModule.forRoot(),
		ServicesModule.forRoot(),
		ReactiveFormsModule,
		TimepickerModule.forRoot()
	], declarations: [
		AppComponent,

		HomePage,
		PageNotFoundPage,
		MembersLoginPage,
		ChangeLogPage,
		ForgotPasswordPage
	], providers: [], schemas: [CUSTOM_ELEMENTS_SCHEMA], bootstrap: [AppComponent]
})
export class AppModule {
}
