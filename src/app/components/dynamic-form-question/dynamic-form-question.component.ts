import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { QuestionBase } from 'app/components/questions/question-base';
import { Router } from '@angular/router';

@Component({
	selector: 'df-question',
	templateUrl: './dynamic-form-question.component.html',
	styleUrls: ['dynamic-form-question.component.css'],
})
export class DynamicFormQuestionComponent implements OnInit {
	@Input() question: QuestionBase<any>;
	@Input() form: FormGroup;

	constructor(private router: Router) {

	}

	ngOnInit() {
		if (this.checkFormControl()) {
			let key = this.question.key;
			let val = this.question.value;
			this.form.get(this.question.key).setValue(this.question.value);
		}
	}

	ngOnChange() {
		console.log('change');
	}

	get isValid() {

		// If disabled, is valid
		if (!!(this.form.get(this.question.key)) && this.form.get(this.question.key).disabled) {
			return true;
		}

		// Fox for required error
		if (!!(this.form.get(this.question.key).errors) && this.form.get(this.question.key).errors.required && this.question.value && this.question.value.length > 0 && !this.form.get(this.question.key).touched) {
			return true;
		}

		// If any step to the control is !null, return .valid property. Else return true
		if (this.checkFormControl()) {
			return this.form.get(this.question.key).valid;
		} else {
			return true;
		}
	}

	get wasTouched() {
		if (!this.checkFormControl()) {
			return false;
		}
		return this.form.get(this.question.key).touched;
	}

	checkFormControl(): boolean {
		return (!!(this.form) && !!(this.question) && !!(this.form.get(this.question.key)));
	}

	redirect() {
		this.router.navigateByUrl(this.question.value);
	}

	get errors(): any {
		if (this.checkFormControl()) {
			return this.form.get(this.question.key).errors;
		} else {
			return null;
		}
	}
}
