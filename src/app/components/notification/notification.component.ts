import { Component, OnInit, Input } from '@angular/core';
import { NotificationModel } from '../../models/notification.model';

/**
 * NotificationComponent is the individual notification
 */
@Component({
	selector: 'app-notification',
	templateUrl: './notification.component.html',
	styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {

	@Input()
	notification : NotificationModel;

	constructor() { }

	ngOnInit() {
	}

}
