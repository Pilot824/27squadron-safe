import { Component } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';

@Component({
	selector: 'app-loading-modal',
	templateUrl: './loading-modal.component.html',
	styleUrls: ['./loading-modal.component.css']
})
export class LoadingModalComponent {

	constructor(public dialogRef: MatDialogRef<LoadingModalComponent>) { }
}
