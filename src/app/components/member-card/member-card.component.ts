import { Component, Input, OnInit } from '@angular/core';
import { UserModel } from '../../models/user.model';
import { environment } from '../../../environments/environment';
import { UserService } from '../../services/user/user.service';

@Component({
	selector: 'app-member-card',
	templateUrl: './member-card.component.html',
	styleUrls: ['./member-card.component.css'],
	providers: [UserService]
})
export class MemberCardComponent implements OnInit {

	@Input() member: UserModel;
	@Input() member_id: number;

	imagePath: string;
	rankString: string;
	showImage: boolean;

	constructor(private userService: UserService) {
		this.showImage = false;
		this.rankString = '';
	}

	ngOnInit() {

		if (typeof this.member_id !== 'undefined') {
			let memberSub = this.userService.getUserById(this.member_id).subscribe(user => {
				if (user != null) {

					this.member = user;

					if (!!(this.member.rank)) {
						this.rankString = this.member.rank.rank_text_full;
					}

					memberSub.unsubscribe();
				}

			});
		}

		if (!!((<any>this.member).rank_text_full)) {
			this.rankString = (<any>this.member).rank_text_full;
		}


		if (this.member.profile_image_filename) {
			this.showImage = true;
			this.imagePath =
				environment.serverBaseURL + '/assets/images/profilepictures/' + this.member.profile_image_filename;
		}

	}

}
