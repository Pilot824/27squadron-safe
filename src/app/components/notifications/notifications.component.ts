import { Component, OnInit } from '@angular/core';
import { NotificationModel } from 'app/models/notification.model';
import { NotificationsService } from 'app/services/notifications/notifications-service.service';

@Component({
	selector: 'app-notifications',
	templateUrl: './notifications.component.html',
	styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {

	notifications : NotificationModel[];

	constructor(private notifService : NotificationsService) { }

	ngOnInit() {
		this.notifService.getNotifications().subscribe( notifs => {
			this.notifications = notifs;
		});
	}

}
