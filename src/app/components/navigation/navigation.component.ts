import { Component, OnInit } from '@angular/core';
import { NavbarLinkModel } from '../../models/navbarlink.model';
import { NavigationService } from '../../services/navigation/navigation.service';
import { Router } from '@angular/router';
import { Globals } from 'app/globals';

/**
 * NavigationComponent generates the main navigation bar at the top of every page.
 */
@Component ( {
	selector    : 'app-navigation',
	templateUrl : './navigation.component.html',
	styleUrls   : [ './navigation.component.css' ],
	providers   : [ NavigationService ]
} )
export class NavigationComponent implements OnInit {

	/**
	 * navigationMenu is the list of navigational items to be displayed from left to right.
	 */
	public navigationMenu : NavbarLinkModel[];
	public versionNumber : string;

	/**
	 * The constructor pairs the NavigationService to the variable navService.
	 * @param navService
	 */
	constructor( private navService : NavigationService ) {
	}

	/**
	 * ngOnInit retrieves the nav list items from the NavigationService, and assigns it to the navigationMenu.
	 */
	ngOnInit() {

		this.navigationMenu = this.navService.getPublicNav();

		this.versionNumber = new Globals().versionNumber;

	}

	/**
	 * For testing purposes, getNavElements retrieves all of the elements in all of the nav variables.
	 * @returns {NavbarLinkModel[]}
	 */
	public getNavElements() : NavbarLinkModel[] {

		let tmp : NavbarLinkModel[];

		tmp = new Array<NavbarLinkModel> ();

		this.navigationMenu.forEach ( item => {
			if ( typeof item.sublinks !== 'undefined' ) {
				item.sublinks.forEach ( subitems => {
					tmp.push ( subitems );
				} );
			} else {
				tmp.push ( item );
			}
		} );

		return tmp;

	}

}
