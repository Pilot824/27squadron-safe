import { QuestionBase } from './question-base';

export class Header1Question extends QuestionBase<string> {
	controlType = 'header1';
	type: string;

	constructor(options: {} = {}) {
		super(options);
	}
}
