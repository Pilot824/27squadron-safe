import { QuestionBase } from './question-base';

export class Header2Question extends QuestionBase<string> {
	controlType = 'header2';
	type: string;

	constructor(options: {} = {}) {
		super(options);
	}
}
