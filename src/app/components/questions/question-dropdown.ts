import { QuestionBase } from './question-base';

export class DropdownQuestion extends QuestionBase<string> {
	controlType = 'dropdown';
	type: string;

	constructor(options: {} = {}) {
		super(options);
	}
}
