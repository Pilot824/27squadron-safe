export class QuestionBase<T>{
	value: T;
	key: string;
	label: string;
	required: boolean;
	order: number;
	controlType: string;
	options: any[];
	disabled: boolean;
	isEmail: boolean;
	hidden: boolean;
	max: boolean;
	min: boolean;
	multiline: boolean;
	color: string;

	constructor(options: {
		value?: T,
		key?: string,
		label?: string,
		required?: boolean,
		order?: number,
		controlType?: string
		options?: any[]
		disabled?: boolean;
		isEmail?: boolean;
		hidden?: boolean;
		max?: boolean;
		min?: boolean;
		multiline?: boolean;
		color?: string;
	} = {}) {
		this.value = options.value;
		this.key = options.key || '';
		this.label = options.label || '';
		this.required = !!options.required;
		this.order = options.order === undefined ? 1 : options.order;
		this.controlType = options.controlType || '';
		this.options = options.options || [];
		this.disabled = !!options.disabled;
		this.isEmail = !!options.isEmail;
		this.hidden = !!options.hidden;
		this.max = !!options.max;
		this.min = !!options.min;
		this.multiline = !!options.multiline;
		this.color = options.color || 'primary';
	}
}
