import { QuestionBase } from './question-base';

export class ButtonQuestion extends QuestionBase<string> {
	controlType = 'button';

	constructor(options: {} = {}) {
		super(options);
	}
}
