import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { EventModel } from '../../models/event.model';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import { environment } from '../../../environments/environment';
import { BaseService } from '../base/base.service';
import { ToastyService } from 'ng2-toasty';
import 'rxjs/add/operator/catch';

/**
 * EventsService will provide the system with the ability to retrieve information regarding events.
 */

@Injectable()
export class EventsService extends BaseService {

	/**
	 * The constructor maps the Http module to the variable, http. Also generates the baseURL
	 */
	constructor(protected http: Http, protected toastyService: ToastyService) {
		super(http, toastyService);
		this.dbUrl = environment.serverBaseURL + '/server/lib/events.php';
	}

	/**
	 * getEvent requests the server to return a single event given the event id
	 * @param eventId
	 * @returns {Observable<EventModel>}
	 */
	getEvent(eventId: number): Observable<EventModel> {
		return this.http.get(this.dbUrl + '?query=getevent&eventid=' + eventId)
			.map((res: Response) => {
				let events = this.yahooFix(res);
				let event = events[0];

				// Fixing booleans
				event.get_availabilities = event.get_availabilities === '1';
				event.parent_permission = event.parent_permission === '1';
				event.is_public = event.is_public === '1';
				event.can_change = event.can_change === '1';
				event.gen_permission_form = event.gen_permission_form === '1'

				return event;
			})
			.catch(this.handleError);
	}

	getEvents(): Observable<EventModel[]> {
		return this.http.get(this.dbUrl + '?query=getevents')
			.map((res: Response) => {
				let responseSplit = res.text().split(`<script type="text/javascript">`);
				return JSON.parse(responseSplit[0]) as any[]
			})
			.catch(this.handleError);
	}

	/**
	 * setEvent calls the db to add the event that is passed into the events table, and it returns the id of the new event.
	 * @param event
	 * @returns {Observable<number>}
	 */
	setEvent(event: EventModel): Observable<any> {

		if (event == null) {
			throw new Error('Event that was passed to the event service was null');
		}

		let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
		let options = new RequestOptions({ headers: headers, method: 'post' });

		return this.http.post(this.dbUrl + '?query=setevent', { event: event }, options)
			.map((res: Response) => {
				try {
					this.yahooFix(res)
				} catch (e) {
					debugger;
				}
				return this.yahooFix(res)
			})
			.catch(this.handleError);
	}

	deleteEvent(event: EventModel) {
		return this.http.get(this.dbUrl + '?query=deleteevent&event_id=' + event.event_id)
			.map((res: Response) => this.yahooFix(res))
			.catch(this.handleError);
	}

	newEvent(event: EventModel): Observable<any> {

		if (event == null) {
			throw new Error('Event that was passed to the event service was null');
		}

		let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
		let options = new RequestOptions({ headers: headers, method: 'post' });

		return this.http.post(this.dbUrl + '?query=setevent', { event: event }, options)
			.map((res: Response) => this.yahooFix(res))
			.catch(this.handleError);
	}

	staffRequirementMet(event: EventModel): boolean {
		// Staffing Requirement Numbers

		// Day
		// ---

		// 1 Staff member / 20 cadets
		// Gender not considered
		// Senior cadets can act as staff (WO2+)

		// Night
		// -----
		// 1 Staff member / 15 cadets
		// Gender Specific
		// Senior cadets MAY act as staff member???

		// 15+ cadets = need staff

		let startDate = new Date(event.start_date_time);
		let endDate = new Date(event.end_date_time);

		let eventIsOvernight: boolean = (startDate.getDay() != endDate.getDay());

		let maleCadets: number = Number.parseInt(event.attending_male_cadets + '');
		let femaleCadets: number = Number.parseInt(event.attending_female_cadets + '');

		let maleStaffCount: number = Number.parseInt(event.attending_male_staff + '');
		let femaleStaffCount: number = Number.parseInt(event.attending_female_staff + '');
		let maleSeniorCount: number = Number.parseInt(event.attending_male_seniors + '');
		let femaleSeniorCount: number = Number.parseInt(event.attending_female_seniors + '');

		maleStaffCount = maleStaffCount ? maleStaffCount : 0;
		femaleStaffCount = femaleStaffCount ? femaleStaffCount : 0;
		maleSeniorCount = maleSeniorCount ? maleSeniorCount : 0;
		femaleSeniorCount = femaleSeniorCount ? femaleSeniorCount : 0;

		if (!eventIsOvernight) {
			let totalCadets: number = maleCadets + femaleCadets;
			let totalStaff: number = maleStaffCount + femaleStaffCount + maleSeniorCount + femaleSeniorCount;

			return Math.ceil(totalCadets / 20) <= totalStaff;
		} else {
			let maleReq = Math.ceil(maleCadets / 15) <= maleStaffCount;
			let femaleReq = Math.ceil(femaleCadets / 15) <= femaleStaffCount;

			return maleReq && femaleReq;
		}
	}

	isSameDay(event: EventModel): boolean {
		let start = new Date(event.start_date_time);
		let end = new Date(event.end_date_time);

		if (start.getFullYear() != end.getFullYear()) {
			return false;
		}
		if (start.getMonth() != end.getMonth()) {
			return false;
		}
		if (start.getDay() != end.getDay()) {
			return false;
		}
		return true;
	}

}
