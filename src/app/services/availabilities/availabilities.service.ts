import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { EventAvailModel } from '../../models/event.model';
import { AvailabilityStatus } from '../../globals';
import { BaseService } from '../base/base.service';
import { ToastyService } from 'ng2-toasty';
import { FacebookService } from 'app/services/facebook/facebook.service';

@Injectable ()
export class AvailabilitiesService extends BaseService {

	constructor( protected http : Http, protected toastyService : ToastyService, protected fb : FacebookService ) {
		super ( http, toastyService );
		this.dbUrl = this.libsUrl + 'availabilities.php';
	}

	/**
	 * Retrieves the availability of one member for one event.
	 * @param member_id
	 * @param event_id
	 * @returns {Observable<R>}
	 */
	getMemberEventAvailability( member_id : number, event_id : number ) : Observable<any[]> {
		return this.http.get ( this.dbUrl + '?query=getavail&member_id=' + member_id + '?event_id=' + event_id )
			.map ( ( res : Response ) => res.json () )
	}

	/**
	 * Retrieves the availability for one member for all of their events.
	 * @param member_id
	 * @returns {Observable<R>}
	 */
	getMemberEventAvailabilities( member_id : number ) : Observable<EventAvailModel[]> {
		if ( member_id == null ) {
			throw new Error ( 'member_id is null! member_id = ' + member_id );
		}
		return this.http.get ( this.dbUrl + '?query=getallavail&member_id=' + member_id )
			.map ( ( res : Response ) => {
				let avails = this.yahooFix(res);

				avails.forEach( avail => {
					avail.can_change = avail.can_change === '1';
					avail.other_cadets_attending = parseInt(avail.other_cadets_attending, 10);
				});


				return avails;
			} );
	}

	/**
	 * sends a GET request to the server to update the users availability status.
	 * @param event_id
	 * @param member_id
	 * @param availStatus
	 * @returns {Observable<R>}
	 */
	availabilityStatusUpdate( event_id : number, member_id : number, availStatus : AvailabilityStatus ) {
		return this.http.get ( this.dbUrl + '?query=setavail&member_id=' + member_id + '&event_id=' + event_id + '&status=' + availStatus )
			.map ( ( res : Response ) => {

				let availInfo = res.json()[0];

				if (availStatus === AvailabilityStatus.NO && typeof availInfo.event_title !== 'undefined') {
					console.warn('Disabling facebook group posting');
					// this.fb.groupPost('A cadet has declined attending the event "' + availInfo.event_title + '". If you are interested in attending, log in and apply!').subscribe();
				}
				return availInfo
			});
	}

	getEventAvails( event_id : number ) : Observable<any[]> {
		return this.http.get ( this.dbUrl + '?query=getavail&event_id=' + event_id )
			.map ( ( res : Response ) => res.json () )
	}

}
