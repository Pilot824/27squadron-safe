import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthUserService } from 'app/services/auth-user/auth-user.service';
import { UserService } from 'app/services/user/user.service';
import 'rxjs/add/operator/switchMap';

@Injectable()
export class AuthStaffService implements CanActivate {

	constructor(private authUserService: AuthUserService, private userService: UserService) { }

	canActivate(): Observable<boolean> {
		return this.authUserService.getUser()
			.switchMap(user => {

				return this.userService.getStaffPositions(user.member_id).map(positions => {
					return positions.length > 0;
				});

			});
	}

}
