import { Injectable } from '@angular/core';
import { UserModel } from '../../models/user.model';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import { BaseService } from '../base/base.service';
import { Observable } from 'rxjs/Observable';
import { ToastyService } from 'ng2-toasty';
import { UserService } from '../../services/user/user.service';
import { RankModel } from 'app/models/rank.model';
import 'rxjs/add/operator/catch';

/**
 * Authenticate User Service is a service that provides the controls for the client side to be able to query the server.
 */
@Injectable()
export class AuthUserService extends BaseService {

	private user: UserModel;
	private member_id: number;
	private token: string;
	private storedMemberValidator: boolean;
	private localStorageTokenString: string;

	/**
	 * Initializes all the variables.
	 */
	constructor(protected http: Http, protected toastyService: ToastyService, private userService: UserService) {
		super(http, toastyService);
		this.dbUrl = this.libsUrl + 'members.php';
		this.localStorageTokenString = 'currentUser';
	}

	getUser(member_id?: number): Observable<UserModel> {

		// if ( member_id === 0 ) {
		// 	throw new Error ( "member_id that was passed in = 0" );
		// }

		if (member_id != null && member_id !== 0) {	// provided member_id
			return this.http.get(this.dbUrl + '?query=getuser&member_id=' + member_id)
				.map((res: Response) => {
					this.user = this.formatUserResponse(res.json()[0]);
					this.member_id = this.user.member_id;
					this.storedMemberValidator = true;
					return this.user;
				});
		} else if (this.user != null && this.storedMemberValidator) {	// Current user
			return new Observable(subscriber => {
				subscriber.next(this.user);
				subscriber.complete();
			});
		} else if (this.token != null) {	// Current token
			return this.http.get(this.dbUrl + '?query=getuser&token=' + this.token)
				.map((res: Response) => {
					this.user = this.formatUserResponse(res.json()[0]);
					this.member_id = this.user.member_id;
					this.storedMemberValidator = true;
					return this.user;
				});
		} else if (this.member_id != null) {	// Current member_id
			return this.http.get(this.dbUrl + '?query=getuser&member_id=' + this.member_id)
				.map((res: Response) => {
					this.user = this.formatUserResponse(res.json()[0]);
					this.member_id = this.user.member_id;
					this.storedMemberValidator = true;
					return this.user;
				});
		} else if (JSON.parse(localStorage.getItem(this.localStorageTokenString)).token) {	// Stored token

			let url = this.dbUrl + '?query=getuser&token=' + JSON.parse(localStorage.getItem(this.localStorageTokenString)).token;

			return this.http.get(url)
				.map((res: Response) => {
					this.user = this.formatUserResponse(res.json()[0]);
					if (typeof this.user === 'undefined') {
						throw new Error('User that was returned was undefined from stored token');
					}
					this.member_id = this.user.member_id;
					return this.user;
				});
		} else {
			throw new Error('Can\'t get user as no information was provided.');
		}

	}

	formatUserResponse(rawUser: any): UserModel {
		return this.userService.formatUserResponse(rawUser);
	}

	getToken(username: string, password: string): Observable<any> {

		let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
		let options = new RequestOptions({ headers: headers, method: 'post' });

		return this.http.post(this.libsUrl + 'login.php?action=getToken',
			{ username: username, password: password }, options)
			.map((res: Response) => {
				let response = this.yahooFix(res);

				if (response.error != null) {
					console.error('Observable error thrown!');
					this.toastyService.error(response.error);
					return Observable.throw(response.error);
				}

				this.member_id = response.member_id;
				this.setToken(response.token);
				return response;
			});
	}


	validateToken(token: string): Observable<boolean> {
		if (token == null) {
			return new Observable((observer) => {
				observer.next(false);
				observer.complete();
			});
		} else {

			let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
			let options = new RequestOptions({ headers: headers, method: 'post' });

			return this.http.post(this.libsUrl + 'login.php?action=validateToken', { token: token }, options)
				.map((res: Response) => {
					let response = res.json();
					// Response is boolean
					if (response.validToken) {
						this.token = token;
					}
					return response.validToken;
				})
				.catch(this.handleError);
		}
	}

	logout(): void {
		delete this.member_id;
		delete this.user;
		localStorage.removeItem(this.localStorageTokenString);
	}

	setToken(token: string): void {
		if (typeof token !== 'string') {
			throw new Error('Token was not a string: ' + token + ' -> ' + typeof token);
		}

		this.token = token;
		localStorage.setItem(this.localStorageTokenString, JSON.stringify({ token: token }));
	}

	invalidateStoredUser() {
		this.storedMemberValidator = false;
	}

}
