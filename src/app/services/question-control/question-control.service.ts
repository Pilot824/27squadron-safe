import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { QuestionBase } from 'app/components/questions/question-base';
import { PasswordValidation } from 'app/validators/password.validator';

@Injectable()
export class QuestionControlService {
	constructor() { }

	toFormGroup(questions: QuestionBase<any>[]) {
		let group: any = {};

		if (questions != null) {

			questions.forEach(question => {

				let validatorsArray = new Array<any>();

				if (question.required) {
					validatorsArray.push(Validators.required);
				}

				if (question.isEmail) {
					validatorsArray.push(Validators.email);
				}

				if (question.key == 'confirmPassword') {
					validatorsArray.push(PasswordValidation.MatchPassword)
				}

				if (question.key != '') {
					group[question.key] = new FormControl({ value: question.value || '', disabled: !!question.disabled }, Validators.compose(validatorsArray))
				}
			});

		}

		return new FormGroup(group);
	}
}
