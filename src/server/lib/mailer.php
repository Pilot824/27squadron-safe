<?php
//Load composer's autoloader
require '../vendor/autoload.php';
// require '../vendor/PHPMailer/src/Exception.php';
// require '../vendor/PHPMailer/src/PHPMailer.php';
// require '../vendor/PHPMailer/src/SMTP.php';
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

function sendMail($to, $subject, $body){
	$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
	try {
	//     //Server settings
		$mail->SMTPDebug = 0;                                 // Enable verbose debug output
		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = 'smtp.ipower.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	//     $mail->Port = 587;                                    // TCP port to connect to

	//     //Recipients
		$mail->setFrom('27noreply@lampert.solutions', '27 RCACS Website');
		$mail->addAddress($to);     // Add a recipient
	//     // $mail->addAddress('ellen@example.com');               // Name is optional
	//     // $mail->addReplyTo('info@example.com', 'Information');
	//     // $mail->addCC('cc@example.com');
	//     // $mail->addBCC('bcc@example.com');

	//     //Attachments
	//     // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
	//     // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

	//     //Content
		$mail->isHTML(true);                                  // Set email format to HTML
		$mail->Subject = $subject;
		$mail->Body    = $body;

		$mail->send();
	} catch (Exception $e) {
		echo 'Message could not be sent.';
		echo 'Mailer Error: ' . $mail->ErrorInfo;
	}
}
