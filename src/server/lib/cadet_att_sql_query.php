SELECT 
attendance_events.level,
attendance_events.po,
attendance_events.trg_date,
(
    SELECT
    GROUP_CONCAT(attendance_cadets.lname SEPARATOR '\n')
    FROM attendance
    LEFT JOIN attendance_cadets ON attendance.cin = attendance_cadets.cin
    WHERE attendance.trg_date = attendance_events.trg_date AND attendance_events.level = attendance_cadets.level AND attendance_events.level = 1
) AS present_lvl_1,
(
    SELECT
    GROUP_CONCAT(attendance_cadets.lname SEPARATOR '\n')
    FROM attendance
    LEFT JOIN attendance_cadets ON attendance.cin = attendance_cadets.cin
    WHERE attendance.trg_date = attendance_events.trg_date AND attendance_events.level = attendance_cadets.level AND attendance_events.level = 2
) AS present_lvl_2,
(
    SELECT
    GROUP_CONCAT(attendance_cadets.lname SEPARATOR '\n')
    FROM attendance
    LEFT JOIN attendance_cadets ON attendance.cin = attendance_cadets.cin
    WHERE attendance.trg_date = attendance_events.trg_date AND attendance_events.level = attendance_cadets.level AND attendance_events.level = 3
) AS present_lvl_3,
(
    SELECT
    GROUP_CONCAT(attendance_cadets.lname SEPARATOR '\n')
    FROM attendance
    LEFT JOIN attendance_cadets ON attendance.cin = attendance_cadets.cin
    WHERE attendance.trg_date = attendance_events.trg_date AND attendance_events.level = attendance_cadets.level AND attendance_events.level = 4
) AS present_lvl_4,
(
    SELECT
    GROUP_CONCAT(attendance_cadets.lname SEPARATOR '\n')
    FROM attendance
    LEFT JOIN attendance_cadets ON attendance.cin = attendance_cadets.cin
    WHERE attendance.trg_date = attendance_events.trg_date AND attendance_events.level = attendance_cadets.level AND attendance_events.level = 5
) AS present_lvl_5
FROM attendance_events
ORDER BY po