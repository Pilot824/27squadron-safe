<?php

$cadetAttendances = Array();
$splitLocations = Array();		// Attendance split locations between cadets

$csv = array_map('str_getcsv', file('CadetAttendanceSummary.csv'));

$week_date = "";

$level_1_po_col = 2;
$level_2_po_col = 4;
$level_3_po_col = 6;
$level_4_po_col = 8;
$level_5_po_col = 10;

$level_1_text = "Level 1";
$level_2_text = "Level 2";
$level_3_text = "Level 3";
$level_4_text = "Level 4";
$level_5_text = "Level 5";

$training_sched = Array();

$week_array = Array();

// Cleaning
for( $i = 0; $i < sizeof($csv); $i++){

	if( $csv[$i][0] == "" ){

		$allBlanks = true;

		for($j = 0; $j < 14; $j++){
			if( $csv[$i][$j] != "" ) $allBlanks = false;
		}

		if($allBlanks){
			array_splice($csv, $i, 1);
			$i -= 1;
		}
	}

	if( $csv[$i][2] == "27 London Squadron, Royal Canadian Air Cadets" ){
		array_splice($csv, $i, 1);
		$i -= 1;
	}

	if( $csv[$i][2] == "Cadet Attendance Summary" ){
		array_splice($csv, $i, 1);
		$i -= 1;
	}

	if( $csv[$i][0] == "Summary of Participation in Mandatory Training" ){
		array_splice($csv, $i, 1);
		$i -= 1;
	}

	if( $csv[$i][0] == "Main Appointment" ){
		array_splice($csv, $i, 1);
		$i -= 1;
	}

	if( $csv[$i][0] == "List of Past Activities in Which the Cadet Was Registered" ){
		array_splice($csv, $i, 1);
		$i -= 1;
	}
}

// Printing CSV Data
// for( $i = 0; $i < sizeof($csv); $i++){
// 	for( $j = 0; $j < sizeof($csv[$i]); $j++){
// 		echo($csv[$i][$j] . ", ");
// 	}
// 	echo("<br />");
// }


// Splitting

for( $i = 0; $i < sizeof($csv); $i++ ){
	if($csv[$i][0] == "Last Name"){
		array_push($splitLocations, $i);
	}
}

// var_dump($splitLocations);

// Soring

for ($i=0; $i < sizeof($splitLocations) - 1; $i++) {

	$cadet_i_offset = 1;
	$cadet_level_i_offset = 2;

	$cadet_lname 	= $csv[$splitLocations[$i] + $cadet_i_offset][0];
	$cadet_fname 	= $csv[$splitLocations[$i] + $cadet_i_offset][5];
	$cadet_rank 	= $csv[$splitLocations[$i] + $cadet_i_offset][11];
	$cadet_cin 		= $csv[$splitLocations[$i] + $cadet_i_offset][7];
	$cadet_level 	= $csv[$splitLocations[$i] + $cadet_level_i_offset][8];

	$cadet = [
		'fname'	=> $cadet_fname,
		'lname'	=> $cadet_lname,
		'rank'	=> $cadet_rank,
		'cin'	=> $cadet_cin,
		'level'	=> $cadet_level,
		'event'	=> Array()
	];

	// var_dump($cadet);
	// echo("<br />");

	// Create an array event entries

	for ($j=$splitLocations[$i] + 4; $j < $splitLocations[$i + 1]; $j++) { 
		$event = [
			"title"		=> $csv[$j][0],
			"date"		=> $csv[$j][4],
			"status"	=> $csv[$j][9]
		];

		// var_dump($event);
		// echo("<br />");

		array_push($cadet['event'], $event);
	}

	// var_dump($cadet);
	// echo("<br />");
	// echo("<br />");

	array_push($cadetAttendances, $cadet);
	
}



// Figuring out Training Plan Dates
$csv_mtp = array_map('str_getcsv', file('masterTrgPlan.csv'));

// Cleaning

array_splice($csv_mtp, 0, 5);

for ($i=0; $i < sizeof($csv_mtp); $i++) { 
	if ( $csv_mtp[$i][1] == "Location" ){
		array_splice($csv_mtp, $i, 1);
		$i -= 1;
	}
	if ( $csv_mtp[$i][1] == "Instructor" ){
		array_splice($csv_mtp, $i, 1);
		$i -= 1;
	}
}

// Finding Days and classes


for ($i=0; $i < sizeof($csv_mtp); $i++) {


	if ( $csv_mtp[$i][0] != "" ){
		$week_date = $csv_mtp[$i][0];
		array_push($week_array, $week_date);
	}

	$trg_l1 = [
		"date" => $week_date,
		"PO" => $csv_mtp[$i][$level_1_po_col],
		"level" => $level_1_text
	];
	$trg_l2 = [
		"date" => $week_date,
		"PO" => $csv_mtp[$i][$level_2_po_col],
		"level" => $level_2_text
	];
	$trg_l3 = [
		"date" => $week_date,
		"PO" => $csv_mtp[$i][$level_3_po_col],
		"level" => $level_3_text
	];
	$trg_l4 = [
		"date" => $week_date,
		"PO" => $csv_mtp[$i][$level_4_po_col],
		"level" => $level_4_text
	];
	$trg_l5 = [
		"date" => $week_date,
		"PO" => $csv_mtp[$i][$level_5_po_col],
		"level" => $level_5_text
	];

	if($trg_l1['PO'] != "") array_push($training_sched, $trg_l1);
	if($trg_l2['PO'] != "") array_push($training_sched, $trg_l2);
	if($trg_l3['PO'] != "") array_push($training_sched, $trg_l3);
	if($trg_l4['PO'] != "") array_push($training_sched, $trg_l4);
	if($trg_l5['PO'] != "") array_push($training_sched, $trg_l5);

}


// // Printing CSV Data
// for( $i = 0; $i < sizeof($csv_mtp); $i++){
// 	for( $j = 0; $j < sizeof($csv_mtp[$i]); $j++){
// 		echo($csv_mtp[$i][$j] . ", ");
// 	}
// 	echo("<br />");
// 	echo("<br />");
// }

// Printing trg_sched

// for ($i=0; $i < sizeof($training_sched); $i++) { 
// 	echo($training_sched[$i]['date'] . " (" . $training_sched[$i]['level'] . "): \"" . $training_sched[$i]['PO'] . "\"");
// 	echo("<br />");
// }


// BUILDING display table

// 1 = table
// 2 = SQL gen 
// 3 = csv

$disp_mode = 2;

if( $disp_mode == 1 ){

	echo("Cadets: " . sizeof($cadetAttendances) . "<br />");

	echo("<h1>Level 1's</h1>");

	echo("<table style='border-style: solid'>");

		echo("<tr style='border-style: solid'>");

			echo("<th style='border-style: solid'>Date</th>");
			echo("<th style='border-style: solid'>PO</th>");

			for ($i=0; $i < sizeof($cadetAttendances); $i++) {
				if($cadetAttendances[$i]['level'] == "Level 1"){
					echo("<th style='border-style: solid'>" . $cadetAttendances[$i]['lname'] . "</th>");
				}
			}


		echo("</tr>");

		for ($i=0; $i < sizeof($training_sched); $i++) {

			echo("<tr>");

				if($training_sched[$i]['level'] == "Level 1"){

					echo("<td style='border-style: solid'>" . $training_sched[$i]['date'] . "</td>");

					echo("<td style='border-style: solid'>" . $training_sched[$i]['PO'] . "</td>");

					for ($j=0; $j < sizeof($cadetAttendances); $j++) {

						if($cadetAttendances[$j]['level'] == "Level 1"){

							$foundStatus = false;

							for ($k=0; $k < sizeof($cadetAttendances[$j]['event']); $k++) { 

								$attendanceDate = strtotime($cadetAttendances[$j]['event'][$k]['date']);
								$trgSchedDate = strtotime($training_sched[$i]['date']);

								if($attendanceDate == $trgSchedDate){

									echo("<td style='border-style: solid'>" . "(" . $cadetAttendances[$j]['lname'] . ")" . $cadetAttendances[$j]['event'][$k]['status'] . "</td>");

									$foundStatus = true;

									$k = sizeof($cadetAttendances[$j]['event']);
								}
							}

							if(!$foundStatus){
								echo("<td style='border-style: solid'>" . "Absent" . "</td>");
							}

							

						}

					}

				}

			echo("</tr>");

			// if($cadetAttendances[$i]['level'] == "Level 1"){
			// 	echo("<td style='border-style: solid'>" . $cadetAttendances[$i]['lname'] . "</td>");
			// }
		}

	echo("</table>");

	// BUILDING display table

	echo("<h1>Level 2's</h1>");

	echo("<table style='border-style: solid'>");

		echo("<tr style='border-style: solid'>");

			echo("<th style='border-style: solid'>Date</th>");
			echo("<th style='border-style: solid'>PO</th>");

			for ($i=0; $i < sizeof($cadetAttendances); $i++) {
				if($cadetAttendances[$i]['level'] == "Level 2"){
					echo("<th style='border-style: solid'>" . $cadetAttendances[$i]['lname'] . "</th>");
				}
			}


		echo("</tr>");

		for ($i=0; $i < sizeof($training_sched); $i++) {

			echo("<tr>");

				if($training_sched[$i]['level'] == "Level 2"){

					echo("<td style='border-style: solid'>" . $training_sched[$i]['date'] . "</td>");

					echo("<td style='border-style: solid'>" . $training_sched[$i]['PO'] . "</td>");

					for ($j=0; $j < sizeof($cadetAttendances); $j++) {

						if($cadetAttendances[$j]['level'] == "Level 2"){

							$foundStatus = false;

							for ($k=0; $k < sizeof($cadetAttendances[$j]['event']); $k++) { 

								$attendanceDate = strtotime($cadetAttendances[$j]['event'][$k]['date']);
								$trgSchedDate = strtotime($training_sched[$i]['date']);

								if($attendanceDate == $trgSchedDate){

									echo("<td style='border-style: solid'>" . "(" . $cadetAttendances[$j]['lname'] . ")" . $cadetAttendances[$j]['event'][$k]['status'] . "</td>");

									$foundStatus = true;

									$k = sizeof($cadetAttendances[$j]['event']);
								}
							}

							if(!$foundStatus){
								echo("<td style='border-style: solid'>" . "Absent" . "</td>");
							}

							

						}

					}

				}

			echo("</tr>");

			// if($cadetAttendances[$i]['level'] == "Level 1"){
			// 	echo("<td style='border-style: solid'>" . $cadetAttendances[$i]['lname'] . "</td>");
			// }
		}

	echo("</table>");

	// BUILDING display table

	echo("<h1>Level 3's</h1>");

	echo("<table style='border-style: solid'>");

		echo("<tr style='border-style: solid'>");

			echo("<th style='border-style: solid'>Date</th>");
			echo("<th style='border-style: solid'>PO</th>");

			for ($i=0; $i < sizeof($cadetAttendances); $i++) {
				if($cadetAttendances[$i]['level'] == "Level 3"){
					echo("<th style='border-style: solid'>" . $cadetAttendances[$i]['lname'] . "</th>");
				}
			}


		echo("</tr>");

		for ($i=0; $i < sizeof($training_sched); $i++) {

			echo("<tr>");

				if($training_sched[$i]['level'] == "Level 3"){

					echo("<td style='border-style: solid'>" . $training_sched[$i]['date'] . "</td>");

					echo("<td style='border-style: solid'>" . $training_sched[$i]['PO'] . "</td>");

					for ($j=0; $j < sizeof($cadetAttendances); $j++) {

						if($cadetAttendances[$j]['level'] == "Level 3"){

							$foundStatus = false;

							for ($k=0; $k < sizeof($cadetAttendances[$j]['event']); $k++) { 

								$attendanceDate = strtotime($cadetAttendances[$j]['event'][$k]['date']);
								$trgSchedDate = strtotime($training_sched[$i]['date']);

								if($attendanceDate == $trgSchedDate){

									echo("<td style='border-style: solid'>" . "(" . $cadetAttendances[$j]['lname'] . ")" . $cadetAttendances[$j]['event'][$k]['status'] . "</td>");

									$foundStatus = true;

									$k = sizeof($cadetAttendances[$j]['event']);
								}
							}

							if(!$foundStatus){
								echo("<td style='border-style: solid'>" . "Absent" . "</td>");
							}

							

						}

					}

				}

			echo("</tr>");

			// if($cadetAttendances[$i]['level'] == "Level 1"){
			// 	echo("<td style='border-style: solid'>" . $cadetAttendances[$i]['lname'] . "</td>");
			// }
		}

	echo("</table>");

	// BUILDING display table

	echo("<h1>Level 4's</h1>");

	echo("<table style='border-style: solid'>");

		echo("<tr style='border-style: solid'>");

			echo("<th style='border-style: solid'>Date</th>");
			echo("<th style='border-style: solid'>PO</th>");

			for ($i=0; $i < sizeof($cadetAttendances); $i++) {
				if($cadetAttendances[$i]['level'] == "Level 4"){
					echo("<th style='border-style: solid'>" . $cadetAttendances[$i]['lname'] . "</th>");
				}
			}


		echo("</tr>");

		for ($i=0; $i < sizeof($training_sched); $i++) {

			echo("<tr>");

				if($training_sched[$i]['level'] == "Level 4"){

					echo("<td style='border-style: solid'>" . $training_sched[$i]['date'] . "</td>");

					echo("<td style='border-style: solid'>" . $training_sched[$i]['PO'] . "</td>");

					for ($j=0; $j < sizeof($cadetAttendances); $j++) {

						if($cadetAttendances[$j]['level'] == "Level 4"){

							$foundStatus = false;

							for ($k=0; $k < sizeof($cadetAttendances[$j]['event']); $k++) { 

								$attendanceDate = strtotime($cadetAttendances[$j]['event'][$k]['date']);
								$trgSchedDate = strtotime($training_sched[$i]['date']);

								if($attendanceDate == $trgSchedDate){

									echo("<td style='border-style: solid'>" . "(" . $cadetAttendances[$j]['lname'] . ")" . $cadetAttendances[$j]['event'][$k]['status'] . "</td>");

									$foundStatus = true;

									$k = sizeof($cadetAttendances[$j]['event']);
								}
							}

							if(!$foundStatus){
								echo("<td style='border-style: solid'>" . "Absent" . "</td>");
							}

							

						}

					}

				}

			echo("</tr>");

			// if($cadetAttendances[$i]['level'] == "Level 1"){
			// 	echo("<td style='border-style: solid'>" . $cadetAttendances[$i]['lname'] . "</td>");
			// }
		}

	echo("</table>");

	// BUILDING display table

	echo("<h1>Level 5's</h1>");

	echo("<table style='border-style: solid'>");

		echo("<tr style='border-style: solid'>");

			echo("<th style='border-style: solid'>Date</th>");
			echo("<th style='border-style: solid'>PO</th>");

			for ($i=0; $i < sizeof($cadetAttendances); $i++) {
				if($cadetAttendances[$i]['level'] == "Level 5"){
					echo("<th style='border-style: solid'>" . $cadetAttendances[$i]['lname'] . "</th>");
				}
			}


		echo("</tr>");

		for ($i=0; $i < sizeof($training_sched); $i++) {

			echo("<tr>");

				if($training_sched[$i]['level'] == "Level 5"){

					echo("<td style='border-style: solid'>" . $training_sched[$i]['date'] . "</td>");

					echo("<td style='border-style: solid'>" . $training_sched[$i]['PO'] . "</td>");

					for ($j=0; $j < sizeof($cadetAttendances); $j++) {

						if($cadetAttendances[$j]['level'] == "Level 5"){

							$foundStatus = false;

							for ($k=0; $k < sizeof($cadetAttendances[$j]['event']); $k++) { 

								$attendanceDate = strtotime($cadetAttendances[$j]['event'][$k]['date']);
								$trgSchedDate = strtotime($training_sched[$i]['date']);

								if($attendanceDate == $trgSchedDate){

									echo("<td style='border-style: solid'>" . "(" . $cadetAttendances[$j]['lname'] . ")" . $cadetAttendances[$j]['event'][$k]['status'] . "</td>");

									$foundStatus = true;

									$k = sizeof($cadetAttendances[$j]['event']);
								}
							}

							if(!$foundStatus){
								echo("<td style='border-style: solid'>" . "Absent" . "</td>");
							}

							

						}

					}

				}

			echo("</tr>");

			// if($cadetAttendances[$i]['level'] == "Level 1"){
			// 	echo("<td style='border-style: solid'>" . $cadetAttendances[$i]['lname'] . "</td>");
			// }
		}

	echo("</table>");

} else if ($disp_mode == 2) {


// 	SELECT 
// attendance_events.po,
// attendance_events.trg_date,
// (
//     SELECT
//     GROUP_CONCAT(attendance.cin SEPARATOR '\n')
//     FROM attendance
//     WHERE attendance.trg_date = attendance_events.trg_date
// ) AS present
// FROM attendance_events


	// Member Table

	$query = "";

	$query .= "DROP TABLE IF EXISTS attendance_cadets; ";
	$query .= "DROP TABLE IF EXISTS attendance_events; ";
	$query .= "DROP TABLE IF EXISTS attendance; ";
	$query .= "CREATE TABLE attendance_events (
		trg_date date, 
		po mediumtext,
		level int(2)
	); ";
	$query .= "CREATE TABLE attendance_cadets (
		cin int(11), 
		fname mediumtext, 
		lname mediumtext, 
		rank mediumtext, 
		level int(2)
	); ";
	$query .= "CREATE TABLE attendance (
		cin int(11), 
		trg_date date, 
		status mediumtext
	); ";

	$query .= "INSERT INTO attendance_cadets VALUES ";

	for ($i=0; $i < sizeof($cadetAttendances); $i++) { 

		$level_numb = substr($cadetAttendances[$i]['level'], strlen($cadetAttendances[$i]['level']) - 1); //substr(string, start);

		$query .= "(" . $cadetAttendances[$i]['cin'] . ", \"" . $cadetAttendances[$i]['fname'] . "\", \"" . $cadetAttendances[$i]['lname'] . "\", \"" . $cadetAttendances[$i]['rank'] . "\", " . $level_numb . "), ";
	}

	$query = substr($query, 0, strlen($query) - 2);

	$query .= "; ";


	$query .= "INSERT INTO attendance_events VALUES ";

	for ($i=0; $i < sizeof($training_sched); $i++) { 

		$level_numb = substr($training_sched[$i]['level'], strlen($training_sched[$i]['level']) - 1); //substr(string, start);

		$query .= "(\"" . date('Y-m-d', strtotime($training_sched[$i]['date'])) . "\", \"" . $training_sched[$i]['PO'] . "\", " . $level_numb . "), ";
	}

	$query = substr($query, 0, strlen($query) - 2);

	$query .= "; ";


	$query .= "INSERT INTO attendance VALUES ";

	for ($i=0; $i < sizeof($cadetAttendances); $i++) {

		for ($j=0; $j < sizeof($cadetAttendances[$i]['event']); $j++) { 
			$query .= "(" . $cadetAttendances[$i]['cin'] . ", \"" . $cadetAttendances[$i]['event'][$j]['date'] . "\", \"" . $cadetAttendances[$i]['event'][$j]['status'] . "\" ), ";
		}

	}

	$query = substr($query, 0, strlen($query) - 2);

	$query .= "; ";



	echo($query);

	// db_query($query);
} else if ($disp_mode == 3){

	$level_numb = "Level 1";

	// Determining number of columns Needed
	// number of cadets + Date + PO
	$col_count =  2;

	for ($i=0; $i < sizeof($cadetAttendances); $i++) {
		if($cadetAttendances[$i]['level'] == $level_numb){
			$col_count++;
		}
	}

	echo('Date,');
	echo('PO');
	for ($i=0; $i < sizeof($cadetAttendances); $i++) {
		if($cadetAttendances[$i]['level'] == $level_numb){
			echo("," . $cadetAttendances[$i]['lname']);
		}
	}


	for ($i=0; $i < sizeof($training_sched); $i++) {

		if($training_sched[$i]['level'] == $level_numb){

			echo('<br />');

			echo($training_sched[$i]['date'] . ",");

			echo($training_sched[$i]['PO']);

			for ($j=0; $j < sizeof($cadetAttendances); $j++) {

				if($cadetAttendances[$j]['level'] == $level_numb){

					$foundStatus = false;

					for ($k=0; $k < sizeof($cadetAttendances[$j]['event']); $k++) { 

						$attendanceDate = strtotime($cadetAttendances[$j]['event'][$k]['date']);
						$trgSchedDate = strtotime($training_sched[$i]['date']);

						if($attendanceDate == $trgSchedDate){

							echo("," . "(" . $cadetAttendances[$j]['lname'] . ")" . $cadetAttendances[$j]['event'][$k]['status']);

							$foundStatus = true;

							$k = sizeof($cadetAttendances[$j]['event']);
						}
					}

					if(!$foundStatus){
						echo("," . "Absent" . "");
					}

					

				}

			}

		}

		// if($cadetAttendances[$i]['level'] == "Level 1"){
		// 	echo("<td style='border-style: solid'>" . $cadetAttendances[$i]['lname'] . "</td>");
		// }
	}



	// echo("Level 1 classes");

	// echo("<h1>Level 1's</h1>");

	// echo("<table style='border-style: solid'>");

	// 	echo("<tr style='border-style: solid'>");

	// 		echo("<th style='border-style: solid'>Date</th>");
	// 		echo("<th style='border-style: solid'>PO</th>");

	// 		for ($i=0; $i < sizeof($cadetAttendances); $i++) {
	// 			if($cadetAttendances[$i]['level'] == "Level 1"){
	// 				echo("<th style='border-style: solid'>" . $cadetAttendances[$i]['lname'] . "</th>");
	// 			}
	// 		}


	// 	echo("</tr>");

	// 	for ($i=0; $i < sizeof($training_sched); $i++) {

	// 		echo("<tr>");

	// 			if($training_sched[$i]['level'] == "Level 1"){

	// 				echo("<td style='border-style: solid'>" . $training_sched[$i]['date'] . "</td>");

	// 				echo("<td style='border-style: solid'>" . $training_sched[$i]['PO'] . "</td>");

	// 				for ($j=0; $j < sizeof($cadetAttendances); $j++) {

	// 					if($cadetAttendances[$j]['level'] == "Level 1"){

	// 						$foundStatus = false;

	// 						for ($k=0; $k < sizeof($cadetAttendances[$j]['event']); $k++) { 

	// 							$attendanceDate = strtotime($cadetAttendances[$j]['event'][$k]['date']);
	// 							$trgSchedDate = strtotime($training_sched[$i]['date']);

	// 							if($attendanceDate == $trgSchedDate){

	// 								echo("<td style='border-style: solid'>" . "(" . $cadetAttendances[$j]['lname'] . ")" . $cadetAttendances[$j]['event'][$k]['status'] . "</td>");

	// 								$foundStatus = true;

	// 								$k = sizeof($cadetAttendances[$j]['event']);
	// 							}
	// 						}

	// 						if(!$foundStatus){
	// 							echo("<td style='border-style: solid'>" . "Absent" . "</td>");
	// 						}

							

	// 					}

	// 				}

	// 			}

	// 		echo("</tr>");

	// 		// if($cadetAttendances[$i]['level'] == "Level 1"){
	// 		// 	echo("<td style='border-style: solid'>" . $cadetAttendances[$i]['lname'] . "</td>");
	// 		// }
	// 	}

	// echo("</table>");

}

?>