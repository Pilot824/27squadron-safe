<?php

function db_connection(){

		$mode = getAccessMode();

		// $mode = 2;

		if($mode == 0){
			// Local host
			$dbServer		= "localhost";
			$dbAccountDB	= "27rcacs_local";
		} else if ($mode == 1){
			// Personal site
			$dbServer		= "lampert.ipowermysql.com";
			$dbAccountDB	= "27rcacs";
		}else if($mode == 2){
			// squadron site
			$dbServer		= "mysql";
			$dbAccountDB	= "phpmyadmin_1";
		}

		$sql = new mysqli($dbServer, $dbAccountUser, $dbAccountPass, $dbAccountDB);

		return $sql;
	}

function db_build_res($mysqli, $q){

	$returnArray = array();

	if ($mysqli->connect_errno) {
		// The connection failed. What do you want to do?
		// You could contact yourself (email?), log the error, show a nice page, etc.
		// You do not want to reveal sensitive information

		// Let's try this:
		echo "Sorry, this website is experiencing problems.";

		// Something you should not do on a public site, but this example will show you
		// anyways, is print out MySQL error related information -- you might log this
		echo "Error: Failed to make a MySQL connection, here is why: \n";
		echo "Errno: " . $mysqli->connect_errno . "\n";
		echo "Error: " . $mysqli->connect_error . "\n";

		// You might want to show them something nice, but we will simply exit
		exit;
	}

	// if( strpos($q, "SET SESSION group_concat_max_len") == 0){

	// 	$mysqli->query($q);
	// 	echo("Calling query");

	// } else
	if (!$result = $mysqli->query($q)) {
		// Oh no! The query failed.
		echo "Sorry, the website is experiencing problems.";

		// Again, do not do this on a public site, but we'll show you how
		// to get the error information
		echo "Error: Our query failed to execute and here is why: \n";
		echo "Query: " . $q . "\n";
		echo "Errno: " . $mysqli->errno . "\n";
		echo "Error: " . $mysqli->error . "\n";
		exit;
	}

	if($result !== true){

		while ($row = $result->fetch_assoc()) {
			array_push($returnArray, $row);
		}

		$result->free();
	}

	return($returnArray);
}


function db_query($q){


	$mysqli = db_connection();

	return db_build_res($mysqli, $q);

	$mysqli->close();


}

function validateToken($token, $member_id){
	$postdata = file_get_contents("php://input");
	$response = json_decode($postdata);

	$token = $response->token;

	$tokenSearchQuery = "SELECT * FROM login_tokens WHERE token = '" . $token . "';";

	$res = db_query($tokenSearchQuery);

	if (sizeof($res) == 1 && $res[0]['token'] == $token){
		return true;
	}
	else return false;

}

function getAccessMode(){
	$mode = 0;
	if($_SERVER["SERVER_NAME"] == "v2.27aircadets.org") $mode = 2;
	else if($_SERVER["SERVER_NAME"] == "localhost") $mode = 0;
	else if($_SERVER["SERVER_NAME"] == "lampert.solutions") $mode = 1;

	return $mode;
}

db_connection()->close();
