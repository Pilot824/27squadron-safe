<?php

include("db_fns.php");
include("member_fns.php");

try {

	if( isset($_GET['query']) ){

		$query = "";

		// Preset Queries
		if( $_GET['query'] == 'update' ){

			$postdata = file_get_contents("php://input");
			$response = json_decode($postdata);

			$member = $response->member;

			if( !isset($member->member_id) ){
				throw new Exception("member member_id was not set");
			}

			$currentLevel = db_query("SELECT * FROM cadet_information WHERE member_id = " . $member->member_id . ";");

			if($member->level != ''){
				if(sizeof($currentLevel) != 0 ){

					$updateCadetInfoQuery = " UPDATE cadet_information SET
						level = " . $member->level . "
						WHERE member_id = " . $member->member_id . ";";

					db_query($updateCadetInfoQuery);

				} else {

					$updateCadetInfoQuery = " INSERT INTO cadet_information SET
						level = " . $member->level . ",
						member_id = " . $member->member_id . ";";

					db_query($updateCadetInfoQuery);
				}
			}

			$query = "UPDATE members SET ";
				if(isset($member->fname))			$query .= "fname = \"" . $member->fname . "\",";
				if(isset($member->lname))			$query .= "lname = \"" . $member->lname . "\",";
				if(isset($member->gender))			$query .= "gender = \"" . $member->gender . "\",";
				if(isset($member->rank_id))			$query .= "rank_id = \"" . $member->rank_id . "\",";
				if(isset($member->email))			$query .= "email = \"" . $member->email . "\",";
				if(isset($member->phone_number))	$query .= "phone_number = \"" . $member->phone_number . "\"";
			$query = rtrim($query,",");
			$query .= " WHERE member_id = " . $member->member_id . ";";

		} else if ($_GET['query'] == 'new'){


			$tmpPassLength = 5;

			$postdata = file_get_contents("php://input");
			$response = json_decode($postdata);

			$member = $response->member;

			if( isset($member->member_id) ){
				throw new Exception("member member_id was set for a new user creation.");
			}

			if( !isset($member->email)){
				throw new Exception("Members email was not provided!");
			}

			if( !isset($member->password)){
				throw new Exception("Members password was not provided!");
			}

			// Checking to make sure email hasn't been used before
			$emlAcctsQuery = "SELECT email FROM members WHERE email = \"" . $member->email . "\"";
			$emlAccts = db_query($emlAcctsQuery);

			if(sizeof($emlAccts) > 0){
				throw new Exception("Another user has that email. Please try again with another email!");
			}

			if(!isset($member->password)){	// Generating random string

				$password = passwordGen();

			} else {
				$password = $member->password;
			}

			$passwordHash = hash("sha256", $password);

			$newUserQuery = "INSERT INTO  members SET ";

			if(isset($member->fname))			$newUserQuery .= " fname = \"" . $member->fname . "\",";
			if(isset($member->lname))			$newUserQuery .= " lname = \"" . $member->lname . "\",";
			if(isset($member->gender))			$newUserQuery .= " gender = \"" . $member->gender . "\",";
			if(isset($member->rank_id))			$newUserQuery .= " rank_id = \"" . $member->rank_id . "\",";
			if(isset($member->email))			$newUserQuery .= " email = \"" . $member->email . "\",";
			if(isset($member->phone_number))	$newUserQuery .= " phone_number = \"" . $member->phone_number . "\",";

			$newUserQuery .= " password = \"" . $passwordHash . "\",
				initialized = 0;";

			db_query($newUserQuery);

			if(!isset($member->password)){
				$query = "SELECT \"" . $randomString . "\" AS tmpPassword;";
			} else {
				$query = "SELECT member_id, fname, lname, email,
					phone_number
				FROM members
				WHERE email=\"" . $member->email . "\"";
			}


		}

		$data = db_query($query);

	} else {

		throw new Exception("No Data");

	}

} catch (Exception $e){
	$data = array("error" => $e->getMessage());
}

	echo(json_encode($data));
