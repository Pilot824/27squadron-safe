<?php

try{

	echo("<p>importing db_fns... ");

	include("db_fns.php");

	echo("Done </p>");

	// Testing import
	echo("<p>Checking db_query... ");
	if(!function_exists("db_query")){
		echo("<p>db_query Function does not exist </p>");
	}
	echo("Done </p>");

	// Mode
	echo("<p> Checking Mode... ");
	$mode = getAccessMode();
	if($mode == 0) echo("Localhost mode</p>");
	else if($mode == 1) echo("Lampert.Solutions mode</p>");
	else if($mode == 2) echo("27rcacs mode</p>");

	// Testing query

	echo("<p>Testing db_query capability... ");
	$memberQuery = "SELECT * FROM members";
	$memberRes = db_query($memberQuery);

	if(sizeof($memberRes) == 0){
		echo("<p>Failed to query member table</p>");
	}

	echo("Done </p>");

} catch(Exception $e){

	echo('Caught error: ' + $e->getMesage() + "\n");

}
