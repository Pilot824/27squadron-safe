<?php

include("db_fns.php");

// Parsing $_POST data

$postdata = file_get_contents("php://input");
$response = json_decode($postdata);

if( isset($_GET['action']) ){

	if($_GET['action'] == "getToken"){

		// Generating and retreiving variables
		$username = $response->username;
		$password = $response->password;
		$tokenHash = hash("sha256", $password . $_SERVER['REQUEST_TIME']);

		// Finding list of users with username nad password hash
		$query =  "SELECT member_id FROM members WHERE email = '" . $username . "' AND password = '" . hash("sha256", $password) . "';";
		$users = db_query($query);

		if( sizeof($users) == 1){
			db_query( "INSERT INTO login_tokens (token, member_id)
				VALUES ('" . $tokenHash . "', ' " . $users[0]['member_id'] . " ');");

			$data = array ("token" => $tokenHash, "member_id" => $users[0]['member_id']);
		} else {
			$data = array("error" => "Bad Username", "query" => $query);
		}

		echo json_encode($data);

	} else if ($_GET['action'] == "validateToken"){

		$token = $response->token;

		$tokenSearchQuery = "SELECT
		login_tokens.token_id,
		login_tokens.member_id,
		login_tokens.token,
		login_tokens.expiary
		FROM login_tokens
		LEFT JOIN members ON members.member_id = login_tokens.member_id
		WHERE login_tokens.token = '" . $token . "'
		AND members.initialized = 1;";

		$res = db_query($tokenSearchQuery);

		if (sizeof($res) == 1 && $res[0]['token'] == $token){
			$data = array("validToken" => true, "member_id" => $res[0]['member_id']);
		}
		else $data = array("validToken" => false);

		echo json_encode ($data);
	}


}
