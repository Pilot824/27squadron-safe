<?php

include("db_fns.php");

try {

	if( !isset($_GET['query']) ){
		throw new Exception("No Data");
	}

	if( $_GET['query'] == 'getavail' ){

		if( isset($_GET['member_id']) && isset($_GET['event_id']) ){

			$query = "SELECT
				availabilities.availability_id,
				availabilities.member_id,
				availabilities.event_id,
				availabilities.status,
				events.get_availabilities,
				events.gen_permission_form
			FROM events
			LEFT JOIN availabilities ON events.event_id = availabilities.event_id
			WHERE events.event_id = " . $_GET['event_id'] . " AND availabilities.member_id = " . $_GET['member_id'];

		} else if (isset($_GET['event_id'])){

			// Returning list of members that have submitted their avails for that event

			$query = "SELECT
				members.member_id,
				members.fname,
				members.lname,
				members.rank_id,
				ranks.rank_text_short,
				ranks.branch,
				availabilities.status
				FROM availabilities
				LEFT JOIN members ON availabilities.member_id = members.member_id
				LEFT JOIN ranks ON members.rank_id = ranks.rank_id
				WHERE availabilities.event_id = " . $_GET['event_id'];

		}
	}

	if( $_GET['query'] == 'getallavail' ){

		if( isset($_GET['member_id']) ){
			$query = "SELECT
				events.event_id,
				events.event_title,
				events.event_description,
				events.max_cadet_count,
				events.start_date_time,
				events.end_date_time,
				events.img_src,
				events.is_public,
				events.parent_permission,
				events.cadet_uniform,
				events.staff_uniform,
				events.get_availabilities,
				events.gen_permission_form,
				COALESCE(availabilities.status, 0) as status,
				events.can_change,
				(
					SELECT
					COUNT(availabilities.status)
					FROM availabilities
					WHERE availabilities.member_id != " . $_GET['member_id'] . "
					AND availabilities.event_id = events.event_id
					AND availabilities.status = 1
				) AS other_cadets_attending
			FROM events
			LEFT JOIN availabilities ON events.event_id = availabilities.event_id
			and availabilities.member_id = " . $_GET['member_id'] . "
			WHERE events.get_availabilities = 1
			AND events.end_date_time >= NOW()
			ORDER BY events.start_date_time";

		}
	}

	if( $_GET['query'] == 'setavail' ){

		if( isset($_GET['member_id']) && isset($_GET['event_id']) && isset($_GET['status']) ){

			// Checks to see if availability is present
			$checkAvailQuery = "SELECT *
			FROM availabilities
			WHERE availabilities.event_id = " . $_GET['event_id'] . " AND availabilities.member_id = " . $_GET['member_id'] . ";";

			$existingAvailCount = db_query($checkAvailQuery);

			$updateQuery = "";

			$query = "SELECT false AS updatedExistingEntry FROM availabilities LIMIT 1;";

			if( sizeof($existingAvailCount) == 0){

				$updateQuery = "INSERT INTO availabilities
				(member_id, event_id, status)
				VALUES
				(" . $_GET['member_id'] . ", " . $_GET['event_id'] . ", " . $_GET['status'] . ");";


				db_query($updateQuery);

			} else if( sizeof($existingAvailCount) == 1){

				// Entry exists, different status => Update existing status and set query to get data
				if( $existingAvailCount[0]['status'] != $_GET['status']){


					$updateQuery = "UPDATE availabilities SET status = " . $_GET['status'] . " WHERE member_id = " . $_GET['member_id'] . " AND event_id = " . $_GET['event_id'] . ";";

					db_query($updateQuery);

					$query = "SELECT
						events.event_title,
						members.fname,
						members.lname,
						members.rank_id
						FROM availabilities
						LEFT JOIN members ON availabilities.member_id = members.member_id
						LEFT JOIN events ON events.event_id = availabilities.event_id
						WHERE events.event_id = " . $_GET['event_id'] . "
						AND members.member_id = " . $_GET['member_id'] . ";";
				}

			}


		}

	}

	if($query == ""){
		throw new Exception("Empty query");
	}

	$data = db_query($query);

} catch (Exception $e){
	$data = array("Error", $e->getMessage());
}

echo(json_encode($data));
