<?php

include("db_fns.php");


try {

	if( isset($_GET['getallactivegroups']) ){

		if(isset($_GET['member_id'])){

			$query = "SELECT
				groups.group_id,
				groups.title,
				groups.description,
				groups.member_id,
				groups.active_start,
				groups.active_end,
				IF(group_membership.member_id IS NULL, FALSE, TRUE) as isMember,
				members.member_id,
				members.fname,
				members.lname,
				members.gender,
				members.rank_id,
				members.email,
				members.phone_number,
				members.element,
				members.branch,
				ranks.rank_text_short,
				ranks.rank_text_full,
				ranks.authority_level,
				ranks.element,
				ranks.branch
			FROM groups
			LEFT JOIN members ON members.member_id = groups.member_id
			LEFT JOIN ranks ON members.rank_id = ranks.rank_id
			LEFT JOIN group_membership ON groups.group_id= group_membership.group_id
				AND group_membership.start_date <= NOW()
				AND group_membership.end_date >= NOW()
				AND group_membership.member_id = " . $_GET['member_id'] . "
			WHERE groups.active_start <= NOW()
			AND groups.active_end >= NOW()";

			$data = db_query($query);

		} else {
			throw new Exception("No Data");
		}

	} else if ( isset ($_GET['updateMembership'])){



	} else {

		throw new Exception("No Data");

	}

} catch (Exception $e){
	$data = array("error" => $e->getMessage());
}

echo(json_encode($data));
