<?php

try{

	include("db_fns.php");
	include("mailer.php");
	include("member_fns.php");

	if( isset($_GET['query']) ){

		$query = "";

		// Preset Queries
		if( $_GET['query'] == 'getuser' ){

			if( isset($_GET['member_id']) ){

				$query = "SELECT
					members.member_id,
					members.fname,
					members.lname,
					members.gender,
					members.rank_id,
					members.email,
					members.phone_number,
					members.element,
					members.branch,
					members.initialized,
					cadet_information.level,
					ranks.rank_id,
					ranks.rank_text_full,
					ranks.rank_text_short,
					ranks.authority_level,
					ranks.element,
					ranks.branch
				FROM members
				LEFT JOIN ranks ON ranks.rank_id = members.rank_id
				LEFT JOIN cadet_information ON cadet_information.member_id = members.member_id
				WHERE members.member_id = " . $_GET['member_id'];
			}

			if( isset($_GET['token']) ){

				$query = "SELECT
					members.member_id,
					members.fname,
					members.lname,
					members.gender,
					members.rank_id,
					members.email,
					members.phone_number,
					members.element,
					members.branch,
					members.initialized,
					cadet_information.level,
					ranks.rank_id,
					ranks.rank_text_full,
					ranks.rank_text_short,
					ranks.authority_level,
					ranks.element,
					ranks.branch
				FROM login_tokens
				LEFT JOIN members ON login_tokens.member_id = members.member_id
				LEFT JOIN ranks ON ranks.rank_id = members.rank_id
				LEFT JOIN cadet_information ON cadet_information.member_id = members.member_id
				WHERE login_tokens.token = \"" . $_GET['token'] . "\";";
			}

		} else if( $_GET['query'] == 'getrank' ){

			if( isset($_GET['rank_id']) ){

				$query = "SELECT
					ranks.rank_id,
					ranks.rank_text_full,
					ranks.rank_text_short,
					ranks.authority_level,
					ranks.element,
					ranks.branch
				FROM ranks WHERE rank_id = " . $_GET['rank_id'];
			}

		} else if ($_GET['query'] == 'getuserbypos'){
			if ( isset($_GET['position_id']) ){
				$query = "SELECT
					members.member_id,
					members.fname,
					members.lname,
					members.gender,
					members.rank_id,
					members.email,
					members.phone_number,
					members.element,
					members.branch,
					members.initialized,
					ranks.rank_id,
					ranks.rank_text_full,
					ranks.rank_text_short,
					ranks.authority_level,
					ranks.element,
					ranks.branch
				FROM members
				LEFT JOIN staff ON members.member_id = staff.member_id
				LEFT JOIN ranks ON members.rank_id = ranks.rank_id
				WHERE (staff.end_date IS NULL OR
				(
					staff.start_date <= NOW() AND staff.end_date >= NOW()
				))
				AND staff.position_id = " . $_GET['position_id'];
			}
		} else if ($_GET['query'] == 'getlistofcurrentusers'){

			$query = "SELECT
					members.member_id,
					members.fname,
					members.lname,
					members.gender,
					members.rank_id,
					members.email,
					members.phone_number,
					members.element,
					members.branch,
					members.initialized,
					cadet_information.level,
					ranks.rank_id,
					ranks.rank_text_full,
					ranks.rank_text_short,
					ranks.authority_level,
					ranks.element,
					ranks.branch
			FROM members
			LEFT JOIN ranks ON members.rank_id = ranks.rank_id
			LEFT JOIN cadet_information on members.member_id = cadet_information.member_id
			WHERE members.active_start <= NOW()
			AND (
				members.active_end IS NULL
				OR members.active_end >= NOW()
			);";

		} else if ($_GET['query'] == 'getranks'){

			$query = "SELECT * FROM ranks";

		} else if ($_GET['query'] == 'staff_positions'){

			if( isset($_GET['member_id'])){
				$query = "SELECT *
				FROM staff
				LEFT JOIN staff_positions ON staff.position_id = staff_positions.position_id
				WHERE staff.member_id = " . $_GET['member_id'] . ";";
						}

		} else if ($_GET['query'] == 'delete_user'){

			throw new Error("INCOMPLETE");

		} else if ($_GET['query'] == 'passwordrecover'){

			if( !function_exists("passwordGen") ) {
				throw new Error("Function password gen was not imported!");
			}

			$postdata = file_get_contents("php://input");
			$response = json_decode($postdata);

			$email = $response->email;

			if(!isset($email)){
				throw new Exception("E-mail was not set");
			}

			$userQuery = "SELECT email FROM members WHERE email = '" . $email . "'";

			$user = db_query($userQuery);

			if(sizeof($user) != 1){
				throw new Error("Could not find entry with that email");
			}

			$newPassword = passwordGen();
			$passwordHash = hash("sha256", $newPassword);

			sendMail(
				$email,
				"27 RCACS Password Recovery",
				"Temporary Password: <b>" . $newPassword . "</b>"
			);

			$query = "UPDATE members SET password = '" . $passwordHash . "' WHERE email = '" . $email . "';";

		} else if ($_GET['query'] == 'setpassword'){

			$postdata = file_get_contents("php://input");
			$response = json_decode($postdata);

			$email = $response->email;
			$member_id = $response->member_id;
			$password = $response->password;

			if(!isset($email) || !isset($member_id) || !isset($password)){
				throw new Exception("Not all information was passed over");
			}

			$userQuery = "SELECT email FROM members WHERE email = '" . $email . "' AND member_id = " . $member_id;

			$user = db_query($userQuery);

			if(sizeof($user) != 1){
				throw new Error("Could not find matching user");
			}

			$passwordHash = hash("sha256", $password);
			$query = "UPDATE members SET password = '" . $passwordHash . "' WHERE email = '" . $email . "' AND member_id = " . $member_id . ";";
		}

		$data = db_query($query);

	} else {

		$data = array("error", "No Data");

	}

} catch (Exception $e){
	$data = array("error" => $e->getMessage());
}

echo(json_encode($data));

?>
