<?php

include("db_fns.php");


try {

	if( isset($_GET['query']) ){

		$query = "";

		// Preset Queries
		if( $_GET['query'] == 'get_initialized' ){

			if(!isset($_GET['member_id'])){
				throw new Exception("No member ID was passed in");
			}

			$query = "SELECT initialized FROM members WHERE member_id = " . $GET_['member_id'] . ";";

		} else if ($_GET['query'] == "initialize") {

			// Making sure all information is passed over
			if( !isset($_GET['token']) ){
				throw new Exception("Token was not provided");
			}
			if( !isset($_GET['member_id']) || $_GET['member_id'] == ''){
				throw new Exception("Member ID was not provide");
			}

			// Check to see if token  belongs to staff member
			$tokenAuthQuery = "SELECT staff.position_id
								FROM staff
								LEFT JOIN login_tokens ON login_tokens.member_id = staff.member_id
								WHERE token = \"" . $_GET['token'] . "\"";

			$tokenAuth = db_query($tokenAuthQuery);

			if(sizeof($tokenAuth) == 0){
				throw new Exception("Error authenticating user");
			} else {
				$query = "UPDATE members SET
					initialized = 1
					WHERE member_id = " . $_GET['member_id'] . ";";
			}
		}

		$data = db_query($query);

	} else {

		throw new Exception("No Data");

	}

} catch (Exception $e){
	$data = array("error" => $e->getMessage());
}

echo(json_encode($data));
