import { browser, element, by } from 'protractor';

export class AboutProgramTestPage {
	navigateTo() {
		return browser.get ( '/about_old/program' );
	}

	getTitle() {
		return element ( by.css ( 'app-root h1' ) ).getText ();
	}
}
