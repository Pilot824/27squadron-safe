import { browser, element, by } from 'protractor';

export class AircadetsPage {
	navigateTo() {
		return browser.get ( '/home' );
	}

	getTitle() {
		return element ( by.css ( 'app-root h1' ) ).getText ();
	}
}
