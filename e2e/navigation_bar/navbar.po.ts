import { browser, element, by } from 'protractor';

export class NavbarTestComponent {
	navigateTo() {
		return browser.get ( "/home" );
	}

	getAllText() {
		return element ( by.css ( 'app-navigation' ) ).getText ();
	}

	clickLink( linkText ) {
		this.getElementByLinkText ( linkText ).click ();
	}

	clickCss( css ) {
		this.getElementByCss ( css ).click ();
	}

	getElementByLinkText( link ) {
		return element ( by.linkText ( link ) );
	}

	getElementByCss( css ) {
		return element ( by.css ( css ) );
	}
}